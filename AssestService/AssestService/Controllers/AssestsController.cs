﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AssestService.Models;
namespace AssestService.Controllers
{
    public class AssestsController : ApiController
    {
        //public IEnumerable<AMtbl> Get()
        //{
        //    using (AMDBEntities entities = new AMDBEntities())
        //        return entities.AMtbls.ToList();
        //}

      //  [Authorize(Users="Amit")]   

     //   [BasicAuthenticationAtrribute]
       [BasicAuthorization]
        public HttpResponseMessage Get(string type = "All")
        {
            using (AMDBEntities entities = new AMDBEntities())
            {
                switch (type.ToLower())
                {
                    case "all":
                        return Request.CreateResponse(HttpStatusCode.OK, entities.AMtbls.ToList());
                    case "hardware":
                        return Request.CreateResponse(HttpStatusCode.OK, entities.AMtbls.Where(e=>e.Type.ToLower()=="Hardware").ToList());
                    case "software":
                        return Request.CreateResponse(HttpStatusCode.OK, entities.AMtbls.Where(e => e.Type.ToLower() == "Software").ToList());
                    case "book":
                        return Request.CreateResponse(HttpStatusCode.OK, entities.AMtbls.Where(e => e.Type.ToLower() == "Book").ToList());
                 
                    default:
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "value should be hardware ,software or hardwarey");
                }
                //    return entities.Employees.ToList();
            }
        }
        [BasicAuthorization]

        public AMtbl Get(int id)
        {
            using (AMDBEntities entities = new AMDBEntities())
            {
                return entities.AMtbls.FirstOrDefault(e=>e.Id==id);
            }
        }

       [BasicAuthorization]
        public HttpResponseMessage Post([FromBody] AMtbl assest)
        {
            try
            {
                using (AMDBEntities entities = new AMDBEntities())

                {
                    entities.AMtbls.Add(assest);
                    entities.SaveChanges();
                    var message = Request.CreateResponse(HttpStatusCode.Created, assest);
                    // message.Headers.Location = new Uri(Request.RequestUri + employee.ID.ToString());
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

        [BasicAuthorization]
        [Route("api/assests/assign/{id}")]

        public HttpResponseMessage Put([FromBody] AMtbl assest, int id)
        {
            try
            {
                using (AMDBEntities entities = new AMDBEntities())
                {
                    var updateee = entities.AMtbls.FirstOrDefault(e => e.Id == id);
                    if (updateee != null)
                    {
                        updateee.Name = assest.Name;
                        updateee.Creator = assest.Creator;
                        updateee.DateOfPublish = assest.DateOfPublish;
                        updateee.AssignedTo = assest.AssignedTo;

                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, "Succesfully updated");
                    }
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Assest with id " + id + " not found ");


                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

         [BasicAuthorization]

        [Route("api/assests/Delete/{id}")]

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                using (AMDBEntities entities = new AMDBEntities()) {
                    entities.AMtbls.Remove(entities.AMtbls.FirstOrDefault(x => x.Id == id));
                    entities.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Succesfully deleetdd");
                }
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [BasicAuthorization]

        [Route("api/assests/assign/{id}")]
        public HttpResponseMessage Put([FromBody] string newasiign, int id)
        {
            try
            {
                using (AMDBEntities entities = new AMDBEntities())
                {
                    var updateee = entities.AMtbls.FirstOrDefault(e => e.Id == id);
                    if (updateee != null)
                    {
                        updateee.AssignedTo = newasiign;

                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, "Succesfully Assigned New person with "+id+" " + newasiign);
                    }
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Assest with id " + id + " not found ");

                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}

