﻿using System;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;
using System.Text;

using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace AssestService
{
    public class BasicAuthenticationAtrribute : System.Web.Http.Filters.ActionFilterAttribute, System.Web.Http.Filters.IAuthenticationFilter
    {
        public void OnAuthentication(HttpActionContext actionContext)
        {   
            if (actionContext.Request.Headers.Authorization == null)
             {
                actionContext.Response = actionContext.Request
                    .CreateResponse(HttpStatusCode.Unauthorized);
            }
            else
            {
                string authenticationToken = actionContext.Request.Headers
                                            .Authorization.Parameter;
                string decodedAuthenticationToken = Encoding.UTF8.GetString(
                Convert.FromBase64String(authenticationToken));
                string[] usernamePasswordArray = decodedAuthenticationToken.Split(':');
                string username = usernamePasswordArray[0];
                string password = usernamePasswordArray[1];
                if (AssestSecurity.Login(username, password))
                {
                    Thread.CurrentPrincipal = new GenericPrincipal(
                        new GenericIdentity(username), null);
                }
                else
                {
                    actionContext.Response = actionContext.Request
                        .CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
        }
        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        //public void OnAuthenticationChallange(AuthenticationChallangeContext actionContext)
        //{

        //}
        //public override void OnAuthorization(HttpActionContext actionContext)
        //{
        //    if (actionContext.Request.Headers.Authorization == null)
        //    {
        //        actionContext.Response = actionContext.Request
        //            .CreateResponse(HttpStatusCode.Unauthorized);
        //    }
        //    else
        //    {
        //        string authenticationToken = actionContext.Request.Headers
        //                                    .Authorization.Parameter;
        //        string decodedAuthenticationToken = Encoding.UTF8.GetString(
        //            Convert.FromBase64String(authenticationToken));
        //        string[] usernamePasswordArray = decodedAuthenticationToken.Split(':');
        //        string username = usernamePasswordArray[0];
        //        string password = usernamePasswordArray[1];

        //        if (AssestSecurity.Login(username, password))
        //        {
        //            Thread.CurrentPrincipal = new GenericPrincipal(
        //                new GenericIdentity(username), null);
        //        }
        //        else
        //        {
        //            actionContext.Response = actionContext.Request
        //                .CreateResponse(HttpStatusCode.Unauthorized);
        //        }
        //    }
        //}
        //public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        //{
        //    throw new NotImplementedException();
        //}
    }
}





//using System;
//using System.Web.Http.Controllers;
//using System.Web.Http.Filters;
//using System.Net.Http;
//using System.Net;
//using System.Text;
//using System.Web.Mvc;
//using System.Security.Principal;
//using System.Threading;

//namespace AssestService
//{
//    public class BasicAuthenticationAtrribute : AuthorizationFilterAttribute
//    {
//        public override void OnAuthorization(HttpActionContext actionContext)
//        {
//            if (actionContext.Request.Headers.Authorization == null)
//            {
//                actionContext.Response = actionContext.Request
//                    .CreateResponse(HttpStatusCode.Unauthorized);
//            }
//            else
//            {
//                string authenticationToken = actionContext.Request.Headers
//                                            .Authorization.Parameter;
//                string decodedAuthenticationToken = Encoding.UTF8.GetString(
//                    Convert.FromBase64String(authenticationToken));
//                string[] usernamePasswordArray = decodedAuthenticationToken.Split(':');
//                string username = usernamePasswordArray[0];
//                string password = usernamePasswordArray[1];

//                if (AssestSecurity.Login(username, password))
//                {
//                    Thread.CurrentPrincipal = new GenericPrincipal(
//                        new GenericIdentity(username), null);
//                }
//                else
//                {
//                    actionContext.Response = actionContext.Request
//                        .CreateResponse(HttpStatusCode.Unauthorized);
//                }
//            }
//        }
//    }
//}
