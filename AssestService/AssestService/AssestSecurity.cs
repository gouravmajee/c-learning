﻿using AssestService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssestService
{
    public class AssestSecurity
    {

        public static bool Login(string username ,string password) {
            using (AMDBEntities entities = new AMDBEntities())
            {
                return entities.Users.Any(e=>e.Username.Equals(username,StringComparison.OrdinalIgnoreCase) 
                &&  e.Password==password);
            }
        }
    }
}