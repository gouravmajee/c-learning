﻿using MyNewAspWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyNewAspWebApi.Controllers
{
    public class NewApiController : ApiController
    {
        practiceEntities db = new practiceEntities();
        [HttpGet]
       public IHttpActionResult Action()
        {
            List<student> obj =db.students.ToList();
            return Ok(obj);
        }

        [HttpGet]
        public IHttpActionResult Action(int id)
        {
            var obj1 = db.students.Where(model => model.std_id == id).FirstOrDefault();
            return Ok(obj1);
        }

    }
}
