﻿using System;

namespace CSVDataImporter.Exceptions
{
    public class NotSupportedExtenstionException :Exception
    {
        public NotSupportedExtenstionException() { }
        public NotSupportedExtenstionException(string message): base(message) { }
    }

}

