using CSVDataDomain;
using DataImporter.Controllers;
using DataImpoterService;
using DataImpoterService.Interface;
using DataImpoterServiceDataImpoterService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using static DataImpoterServiceDataImpoterService.CSVDataImporterService;

namespace DataImporter
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMvc();
           services.AddScoped<ICSVDataImporterService, CSVDataImporterService>();
        }

  
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile(MyLiteralStrings.LoggerPath);
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

        app.UseRouting();

            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}




