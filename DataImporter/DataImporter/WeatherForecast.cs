using System;

namespace DataImporter
{
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }
    }
}


//[
//  { 
//    Project : "HPSC",
//    Manager : "AMulya",
//    Team :{
//            "Gourav" :{
//                   stack:"DOtnet",
//                   position: "",
//                }

//        }
//    },
    
//  {
//Project: "GAIA",
//    Manager: "VArun",
//    Team:
//    {
//    Name1:
//        {
//        stack: "",
//        position: "",
//       }
//    }
//},
//]