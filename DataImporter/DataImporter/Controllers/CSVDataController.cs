﻿using DataImpoterService.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DataImporter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CSVDataController : ControllerBase
    {
        private readonly ICSVDataImporterService _business;
        public CSVDataController(ICSVDataImporterService business) {
            _business = business;
        }

        [HttpGet]
        public IEnumerable<IEnumerable<string>> Get()
        {
            return _business.Get();
        }


        [HttpPost]
        public IActionResult GetCSVData( IFormFile file)
        {
            var result = _business.Post(file);
            return Ok(result);
        }


        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {

        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {

        }

    }
}

