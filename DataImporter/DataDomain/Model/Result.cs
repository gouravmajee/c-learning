﻿using CSVDataDomain;
using System.Collections.Generic;

namespace CSVDataDomain.Model
{
    public class Result
    {
        public List<DataModel> Data { get; set; }
        public bool ErrorStatus { get; set; }
    }
}
