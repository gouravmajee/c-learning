﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSVDataDomain
{
    public class DataModel
    {
        public string Country { get; set; }
        public string Sku { get; set; }
        public string Qty { get; set; } 
    }
}
