﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace BusinessLayer.Interface
{
    public interface IBusiness
    {
        IEnumerable<IEnumerable<string>> Get();
        IEnumerable<IEnumerable<string>> Post(IFormFile file);
        void Put(int id);
        void Delete(int id);

    }
}
