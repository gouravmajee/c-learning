﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
//using System.Web.Mvc;

using CSVDataDomain.Model;

namespace DataImpoterService.Interface
{
    public interface ICSVDataImporterService  
    {
        IEnumerable<IEnumerable<string>> Get();
        List<Result> Post(IFormFile file);
        void Put(int id);
        void Delete(int id);
    }
}
