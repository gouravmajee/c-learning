﻿using BusinessLayer.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BusinessLayer
{
    public class Business : IBusiness
    {
        Validations Validate = new Validations();
        Helper Helperfunctions = new Helper();


        private readonly ILogger<Business> _logger;
        public Business(ILogger<Business> logger)
        {
            _logger = logger;
        }

        public IEnumerable<IEnumerable<string>> Get()
        {
             _logger.LogInformation("---- LOGGER ------");
            List<List<string>> BData = new List<List<string>>();
            //List<string> BCountry = new List<string>(), BSKU = new List<string>(), BQty = new List<string>();
            //List<List<string>> Errors = new List<List<string>>();

            //var path = @"C:\Users\gourav.majee\Desktop\API\DataImporter\DataImporter\DATA.csv";

            //var extension = Path.GetExtension(path);
            //bool ok = path.EndsWith(".csv", StringComparison.Ordinal);
            //using (var reader = new StreamReader(path))
            //{
            //    while (!reader.EndOfStream)
            //    {
            //        var line = reader.ReadLine();
            //        var values = line.Split(',');

            //        BCountry.Add(values[0]);
            //        BSKU.Add(values[1]);
            //        BQty.Add(values[2]);
            //    }
            //}

            //BData = Helperfunctions.AllDataHelper( BCountry, BSKU, BQty);

            return BData;
        }

        public IEnumerable<IEnumerable<string>> Post(IFormFile file)
        {
            _logger.LogInformation("👌 POST method Entered");
            List<List<string>> Data = new List<List<string>>();
            List<string> Country = new List<string>(), SKU = new List<string>(), Qty = new List<string>();
            List<String> HeadersRow = new List<string>();

            if (Path.GetExtension(file.FileName) == ".csv")
            {
                _logger.LogInformation("👌 FILE Format valication - Correct");
                using (var reader = new StreamReader("DATA.csv"))
                {
                    _logger.LogInformation("👌 FILE reading Entered");
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');
                        Country.Add(values[0]);
                        SKU.Add(values[1]);
                        Qty.Add(values[2]);
                    }
                }
                HeadersRow = Helperfunctions.GetAllHeader(Country[0], SKU[0], Qty[0]);
                Data = Helperfunctions.AllDataHelper(Country, SKU, Qty);
            }

            if (!Validate.TotalRowsCountValidation(Data)) {
                _logger.LogError($"❌ Rows exceeed 50 rows)");
            }
            // in hand
            // HeadersRow(1d) Data(2d) Country(1d) SKU(1d(1d)

            // actual flow
            _logger.LogInformation("👌 FLOW STARTED");

            for (int rows = 0; rows < Data.Count; rows++)
            {
                if (rows == 0) //first row headers
                {
                    if (!Validate.HeadersValidation(HeadersRow)) {
                        _logger.LogError($"❌ Headers had Error  : ( {string.Join(" ", HeadersRow)} )");
                    }
                }
                else {
                for (int col = 0; col < Data[rows].Count; col++)
                {
                        var roaskijankai = rows;

                    bool flag = true;
                  //  string dodod = Data[rows][col];
                  //if data in that row col exists
                    if(Data[rows][col] != null)
                    {
                       
                        if (Validate.isValidCOuntryCode(Data[rows][0])){
                               var oo= Data[rows][0];
                             _logger.LogError($"👌 Country {Data[rows][0]} is valid 👌 ");
                        }
                        else{
                                  var oo= Data[rows][0];
                            _logger.LogError($"❌ Country {Data[rows][0]} is not valid ❌");
                            flag = false;
                        }

                        if (Validate.isValidSKU(Data[rows][1])){
                            _logger.LogError($"👌 SKU {Data[rows][1]} is valid 👌 ");
                        }
                        else{
                            _logger.LogError($"❌ SKU {Data[rows][1]} is not valid ❌");
                            flag = false;
                        }

                        if (Validate.isValidQty(Data[rows][2])){
                            _logger.LogError($"👌 Qty {Data[rows][2]} is valid 👌 ");
                        }
                        else
                        {
                            _logger.LogError($"❌ Qty {Data[rows][2]} is not valid ❌");
                            flag = false;
                        }

                        if (!flag)
                        {
                            _logger.LogError($"🙃🙃 one of the row feild was incorrect 🙃🙃");

                        }



                    }
                    //  _logger.LogInformation($"⚠️ Remaining data is {dodod} ");
                }
                }
            }

            //for (int rows = 0; rows < Data.Count; rows++)
            //{

            //    if (rows == 0 && Validate.HeadersValidation(HeadersRow))
            //    {
            //        string ok = "headers are ok";
            //    }
            //    else
            //    {
            //        string ok = "headers are NOT ok";
            //        _logger.LogInformation("Headers missing something");
            //    }

            //}

            _logger.LogInformation("👌 DATA array is formed");

            if (Validate.HeadersValidation(HeadersRow))
            {
                string ok = "headers are ok";
            }
            if (Validate.TotalRowsCountValidation(Data))
            {
                string ok = "Total Rows validation";
            }
            if (Validate.isValidCOuntryCode(Country[2]))
            {
                string okdsds = " Country validation";
            }
            if (Validate.isValidSKU(SKU[2]))
            {
                string okdsds = "SKU  validation succesfull";
            }
            if (Validate.isValidQty(Qty[2]))
            {
                string okdsds = " Qty validation succesfull";
            }

            string[] flattened = Data.SelectMany(x => x).ToArray();
            _logger.LogInformation("👌 Log stuff: {MyArray}", (object)flattened);

            _logger.LogInformation("👌 END\n");
            return Data;
        }

        public void Put(int id)
        {
        }

        public void Delete(int id)
        {
        }
    }
}



/*
 using BusinessLayer.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Serilog;
using System;
using System.Linq;

namespace BusinessLayer
{
    public class Business :IBusiness
    {
        Validations Validate = new Validations();
        Helper Helperfunctions = new Helper();


        private readonly ILogger<Business> _logger;

        public Business(ILogger<Business> logger)
        {
            _logger = logger;
            _logger.LogInformation("---- LOGGER ------");
        }

        public IEnumerable<IEnumerable<string>> Get()
        {
            List<List<string>> BData = new List<List<string>>();
            List<string> BCountry = new List<string>(), BSKU = new List<string>(), BQty = new List<string>();
            List<List<string>> Errors = new List<List<string>>();

            var path = @"C:\Users\gourav.majee\Desktop\API\DataImporter\DataImporter\DATA.csv";

            var extension = Path.GetExtension(path);
            bool ok = path.EndsWith(".csv", StringComparison.Ordinal);
            using (var reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    BCountry.Add(values[0]);
                    BSKU.Add(values[1]);
                    BQty.Add(values[2]);
                }
            }

            BData = Helperfunctions.AllDataHelper( BCountry, BSKU, BQty);

            return BData;
        }

        public IEnumerable<IEnumerable<string>> Post(IFormFile file)
        {
            _logger.LogInformation("POST method Entered");
            List<List<string>> Data = new List<List<string>>();
            List<string> Country = new List<string>(), SKU = new List<string>(), Qty = new List<string>();
            List<String> HeadersRow = new List<string>();

            if (Path.GetExtension(file.FileName) == ".csv")
            {
                _logger.LogInformation("FILE Format valication - Correct");
                using (var reader = new StreamReader("Data.csv"))
                {
                    _logger.LogInformation("FILE reading Entered");
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');
                        Country.Add(values[0]);
                        SKU.Add(values[1]);
                        Qty.Add(values[2]);
                    }
                }
               HeadersRow = Helperfunctions.GetAllHeader(Country[0],SKU[0],Qty[0]);
               Data = Helperfunctions.AllDataHelper(Country, SKU, Qty);
 
            }
            _logger.LogInformation("DATA array is formed");

            if (Validate.HeadersValidation(HeadersRow))
            { 
                string ok = "headers are ok";
            }
            if (Validate.TotalRowsCountValidation(Data))
            {
                string ok = "Total Rows validation";
            }
            if (Validate.isValidCOuntryCode(Country[2]))
            {
                string okdsds = " Country validation";
            }
            if (Validate.isValidSKU(SKU[2]))
            {
                string okdsds = "SKU  validation succesfull";
            }
            if (Validate.isValidQty(Qty[2]))
            {
                string okdsds = " Qty validation succesfull";
            }

           // string[] to = Data.ConvertAll<string>().ToList();
            // _logger.LogInformation(Data.ToString());

          //  _logger.LogInformation("Log stuff: {MyArray}", (object)Headers);


            string[] flattened = Data.SelectMany(x => x).ToArray();
            _logger.LogInformation("Log stuff: {MyArray}", (object)flattened);

            _logger.LogInformation("END");
            return Data;
        }

        public void Put(int id)
        {
        }

        public void Delete(int id)
        {
        }
    }
}

 */