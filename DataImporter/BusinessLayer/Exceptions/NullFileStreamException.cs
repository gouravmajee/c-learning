﻿using System;

namespace CSVDataImporter.Exceptions
{

    public class NullFileStreamException : Exception
    {
        public NullFileStreamException() { }

        public NullFileStreamException(string message)
            : base(message) { }
    }
}

