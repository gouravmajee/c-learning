﻿using System;

namespace CSVDataImporter.Exceptions
{

    public class ColumnsExceededException : Exception
    {
        public ColumnsExceededException() { }

        public ColumnsExceededException(string message)
            : base(message) { }
    }
}

