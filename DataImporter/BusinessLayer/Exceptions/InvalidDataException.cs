﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSVDataImporterServices.Exceptions
{
    public class InvalidDataException : Exception
    {
        public InvalidDataException() { }

        public InvalidDataException(string message)
            : base(message) { }
    }
}
