﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSVDataImporterServices.Exceptions
{
    public class StreamReaderException : Exception
    {
        public StreamReaderException() { }

        public StreamReaderException(string message)
            : base(message) { }
    }
}
