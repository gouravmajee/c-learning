﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSVDataImporterServices.Exceptions
{
    public class SingleRowDataAdderErrorException : Exception
    {
        public SingleRowDataAdderErrorException() { }

        public SingleRowDataAdderErrorException(string message)
            : base(message) { }
    }
}
