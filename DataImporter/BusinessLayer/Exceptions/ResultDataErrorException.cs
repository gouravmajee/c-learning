﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSVDataImporterServices.Exceptions
{
    public class ResultDataErrorException : Exception
    {
        public ResultDataErrorException() { }

        public ResultDataErrorException(string message)
            : base(message) { }
    }
}
