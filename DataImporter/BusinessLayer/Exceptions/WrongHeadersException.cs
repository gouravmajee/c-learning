﻿using System;

namespace CSVDataImporter.Exceptions
{

    public class WrongHeadersException : Exception
    {
        public WrongHeadersException() { }

        public WrongHeadersException(string message)
            : base(message) { }
    }
}

