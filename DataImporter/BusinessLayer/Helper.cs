﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public class Helper
    {
        public List<List<String>> AllDataHelper(List<String> BCountry, List<String> BSKU, List<String> BQty)
        {
            List<List<String>> Alldata = new List<List<string>>();
            Alldata.Add(BCountry);
            Alldata.Add(BSKU);
            Alldata.Add(BQty);
            return Alldata;
        }
        public List<String> GetAllHeader(string HeaderCountry, string HeaderSKU, string HeaderQty)
        {
            List<String> headerArr = new List<string>();
            headerArr.Add(HeaderCountry);
            headerArr.Add(HeaderSKU);
            headerArr.Add(HeaderQty);
            return headerArr;
        }
    }
}
