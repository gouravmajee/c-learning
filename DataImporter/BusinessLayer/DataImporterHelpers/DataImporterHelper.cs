﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataImpoterService.DataImporterHelpers
{
    public static class DataImporterHelper
    {
        public static List<List<String>> AllDataHelper(List<String> BCountry, List<String> BSKU, List<String> BQty)
        {
            List<List<String>> Alldata = new List<List<string>>();
            Alldata.Add(BCountry);
            Alldata.Add(BSKU);
            Alldata.Add(BQty);
            return Alldata;
        }
        public static List<String> GetAllHeader(string HeaderCountry, string HeaderSKU, string HeaderQty)
        {
            List<String> headerArr = new List<string>();
            headerArr.Add(HeaderCountry);
            headerArr.Add(HeaderSKU);
            headerArr.Add(HeaderQty);
            return headerArr;
        }

        public static  List<String> TempDataAccumulator(ref List<String> tempdata, List<String> SingleRowData, int current)
        {
            tempdata.Add(SingleRowData[current + 0]);
            tempdata.Add(SingleRowData[current + 1]);
            tempdata.Add(SingleRowData[current + 2]);
     
            return tempdata;
        }

        public static List<String> SingleRowDataAdder(ref List<String> SingleRowData, String firstCell, string SecondCell, string ThirdCell)
        {
            SingleRowData.Add(firstCell);
            SingleRowData.Add(SecondCell);
            SingleRowData.Add(ThirdCell);
            return SingleRowData;
        }

    }
}
