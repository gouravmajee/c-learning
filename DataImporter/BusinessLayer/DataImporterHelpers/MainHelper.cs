﻿using System.IO;
using CSVDataDomain;
using CSVDataDomain.Model;
using DataImpoterService.DataImporterHelpers;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using static DataImpoterServiceDataImpoterService.CSVDataImporterService;
using CSVDataImporter.Exceptions;
using CSVDataImporterServices.Exceptions;
using Serilog;
namespace DataImporterService.DataImporterHelpers
{
    public class MainHelper
    {
   
        public static bool ValidateFile(IFormFile file )
        {
          //   Log.Information("Supressed Information for errors");

            if (!DataImporterValidations.ValidateExtension(file ))
                return false;

            if (!DataImporterValidations.ValidationsBeforeReadingFile(file ))
                return false;
            return true;
        }


        //returns a result data of Result list 
        public static List<Result> ParseFile(IFormFile file )
        {
            List<List<string>> Data = new List<List<string>>();
            List<string> RowsData = new List<string>();
            bool isLoopGoingToBeVisited = true;

            List<string> HeadersRow = new List<string>() {
                            MyLiteralStrings.countryName,
                            MyLiteralStrings.skuName,
                            MyLiteralStrings.quantityName
                        };

            Data = ReadFile(file);

            try
            {
                for (int rows = 0; rows < Data.Count - 1; rows++)
                {
                    for (int col = 0; col < Data[0].Count; col++)
                    {
                        if (rows == 0) //first row headers
                        {
                            if (!DataImporterValidations.HeadersValidation(HeadersRow) && isLoopGoingToBeVisited)
                            {
                                Log.Error($" ❌ Headers had Error  : ( {string.Join(" ", HeadersRow)} )");
                                isLoopGoingToBeVisited = false;
                            }
                        }
                        else
                        {
                            if (col != 0) //prevent first row header colms to add
                                DataImporterHelper.SingleRowDataAdder(ref RowsData, Data[0][col], Data[1][col], Data[2][col]);
                        }
                    }
                }
            }
            catch
            {
                throw new SingleRowDataAdderErrorException("Error while Adding single datas in RowsData");
            }
            List<Result> ResultData = new List<Result>();

            try
            {
            for (int current = 0, cnt = 0; current < RowsData.Count; current += 3, cnt += 1) {

                List<DataModel> skudata = new List<DataModel>();
                List<string> tempdata = new List<string>(); bool isError = true;

                DataImporterHelper.TempDataAccumulator(ref tempdata, RowsData, current);

                    if (!DataImporterValidations.singleRowFieldsValidation(tempdata))
                    {
                        Log.Error($" ❌ Row {cnt} Missing something ( {string.Join(" ", tempdata)} ‍‍)");

                        if (!DataImporterValidations.isValidCOuntryCode(tempdata[0]))
                            Log.Error($"     ➤ code is wrong ({ tempdata[0]})💀💀");

                        if (!DataImporterValidations.isValidSKU(tempdata[1]))
                            Log.Error($"     ➤ SKU code is wrong ({ tempdata[1]})💀💀");

                        if (!DataImporterValidations.isValidQty(tempdata[2]))
                            Log.Error($"     ➤  Qty is wrong ({ tempdata[2]})💀💀");
                    }
                    else
                    {
                        Log.Information($" ✓ Row {cnt}  is correct which is ({string.Join(" ", tempdata)})🍉");
                        isError = false;
                    }

                    var singledata = new DataModel()
                {
                    Country = RowsData[current + 0],
                    Sku = RowsData[current + 1],
                    Qty = RowsData[current + 2]
                };

                skudata.Add(singledata);

                var TotalData = new Result()
                {
                    Data = skudata,
                    ErrorStatus = isError
                };
                ResultData.Add(TotalData);
            }
            }
            catch
            {
                throw new ResultDataErrorException("Result Data Exception");
            }

            return ResultData;
        }

        public static List<List<string>> ReadFile(IFormFile file)
        {
            List<string> Country = new List<string>(), SKU = new List<string>(), Qty = new List<string>();

            try
            {
            using (var reader = new StreamReader(file.FileName))
            {
                while (!reader.EndOfStream)
                {
                    var stream="";
                    try {
                        stream = reader.ReadLine();
                        var row = stream.Split(MyLiteralStrings.CommmaString);
                        Country.Add(row[0].ToUpper());
                        SKU.Add(row[1].ToUpper());
                        Qty.Add(row[2].ToUpper());
                    }
                    catch
                    {
                        throw new NullFileStreamException("Null stream or Empty line");
                    }
                }
            }
            }
            catch
            {
                throw new StreamReaderException("Stream reader couldnt start");
            }
            
           var returnedData= DataImporterHelper.AllDataHelper(Country, SKU, Qty);
            return returnedData;
        }
    }
}
