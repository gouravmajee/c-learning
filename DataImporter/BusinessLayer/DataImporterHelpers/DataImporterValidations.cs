﻿using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DataImpoterServiceDataImpoterService;
using Microsoft.Extensions.Logging;
using static DataImpoterServiceDataImpoterService.CSVDataImporterService;
using System.Runtime.Serialization;
using CSVDataImporter.Exceptions;
using DataImporterService.DataImporterHelpers;
using Serilog;
namespace DataImpoterService.DataImporterHelpers
{
    public static class DataImporterValidations
    {
        public static bool ValidateExtension(IFormFile file )
        {
            //  EXtension check
            try
            {
                if (MyLiteralStrings.csvExtenstion != Path.GetExtension(file.FileName))
                {
                    Log.Error("Extension check failed");

                    return false;
                }
                Log.Information("Extension check correct");
                return true;
            }
            catch
            {
              throw new NotSupportedExtenstionException("Extension check failed");
            }
        }

        //ValidateCountry ValidateSKU ValidateQty
        public static bool ValidationsBeforeReadingFile(IFormFile file )
        {
            //  EXtension check
            List<List<string>> Data = new List<List<string>>();
            List<string> HeadersRow = new List<string>() ;

            Log.Information("validate rows entered");

            //TODO readFile()
            Data = MainHelper.ReadFile(file);
            HeadersRow =  new List<string>() { MyLiteralStrings.countryName , MyLiteralStrings.skuName , MyLiteralStrings.quantityName };

            //  Headers correct check 
            if (!DataImporterValidations.HeadersValidation(HeadersRow))
            {
                Log.Error("Headers are Wrong OR Order is Wrong");
                return false;
            }

            //  ROws correct check 
            try
            {
                if (!DataImporterValidations.TotalRowsCountValidation(Data))
                {
                  Log.Error($"❌ Rows exceeed 50 rows)");
                    return false;
                }
                return true;
            }
            catch
            {
                throw new ColumnsExceededException("Detected ColumnsExceededException");
            }
        }

        public static bool HeadersValidation(List<String> incomingHeaders)
        {
            string[] Headers = new string[] { MyLiteralStrings.countryName, MyLiteralStrings.skuName, MyLiteralStrings.quantityName };
            for (int i = 0; i < 2; i++)
            {
                if (incomingHeaders[i] != Headers[i])
                    return false;
            }
            return true;
        }

        public static bool TotalRowsCountValidation(List<List<String>> incomingData)
        {
            try{
                if (incomingData[0] != null){
                if (incomingData[0].Count() <= 50)
                    return true;
                 }
            }
            catch{
                throw new InvalidDataException("Data invalid /Null found");
            }
            return false;
        }

        public static bool isValidCOuntryCode(string str) {
            string strRegex = @MyLiteralStrings.countryCodeRegularExpression;
            Regex re = new Regex(strRegex);
            return re.IsMatch(str);
        }

        public static bool isValidSKU(string str)
        {
            string strRegex = MyLiteralStrings.skuRegularExpression;
            Regex re = new Regex(strRegex);
            bool flag = re.IsMatch(str);
            return re.IsMatch(str);
        }

        public static bool isValidQty(string str)
        {
            string strRegex = MyLiteralStrings.validQtyRegularExpression;
            Regex re = new Regex(strRegex);
            bool flag = re.IsMatch(str);
            return re.IsMatch(str);
        }
       
        public static bool singleRowFieldsValidation(List<String> data)
        {
            if (isValidCOuntryCode(data[0]) && isValidSKU(data[1])  && isValidQty(data[2]))
                return true;
            return false;
        }
    }

    
}
