﻿using SimpleFactory;
using SimpleFactory.Product;
using System;

namespace CreationalDesignPattern
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //IMobile mobile = MobileFactory.CreateMobile(BrandType.Xiomi);
            //mobile.GetMobile();

            //IMobile applekaphone = MobileFactory.CreateMobile(BrandType.Apple);
            //applekaphone.GetMobile();

            //IMobile mobile1 = MobileFactory.CreateMobile(BrandType.Xiomi);
            //mobile1.GetStatus();

            ILaptop lappy1 = LaptopFactory.CreateLaptop(BrandType.Xiomi);
            lappy1.Getlaptop(); 

            //Console.WriteLine("Hello World!");
        }
    }
}
