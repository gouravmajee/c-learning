﻿using SimpleFactory.Product;
using System;

namespace SimpleFactory.ConcreteProduct
{
    public class Xiomi : IMobile
    {
        public void GetMobile()
        {
            Console.WriteLine("Xiomi mobile was created");
        }

        public void GetStatus()
        {
            Console.WriteLine("world best phone haiiiii");

        }
    }
}
