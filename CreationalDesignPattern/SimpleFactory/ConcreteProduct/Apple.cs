﻿using SimpleFactory.Product;
using System;

namespace SimpleFactory
{
    public class Apple : IMobile
    {
        public void GetMobile()
        {
            Console.WriteLine("APple mobile was created");
        }
        public void GetStatus()
        {
            Console.WriteLine("Ameer");
        }
    }
}