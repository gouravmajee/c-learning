﻿using SimpleFactory.Product;
using System;

namespace SimpleFactory.ConcreteProduct
{
    public class Macbook : ILaptop
    {
        public void Getlaptop()
        {
            Console.WriteLine("this is macbook");
        }
    }
}
