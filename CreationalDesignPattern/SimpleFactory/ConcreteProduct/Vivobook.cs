﻿using SimpleFactory.Product;
using System;

namespace SimpleFactory.ConcreteProduct
{
    public class Vivobook : ILaptop
    {
        public void Getlaptop()
        {
            Console.WriteLine("this is vivobook");
        }
    }
}
