﻿namespace SimpleFactory.Product
{
    public interface IMobile
    {
          void GetMobile();
          void GetStatus();
    }
}
