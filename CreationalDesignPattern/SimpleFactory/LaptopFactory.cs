﻿using SimpleFactory.ConcreteProduct;
using SimpleFactory.Product;
using System;

namespace SimpleFactory
{
    public class LaptopFactory
    {

        public static ILaptop CreateLaptop(BrandType brandType)
        {
            switch (brandType)
            {
                case BrandType.Macbook:
                    return new Macbook();
                case BrandType.Vivobook:
                    return new Vivobook();
                default:
                    throw new Exception("invalid type");
            }

        }
    }
}
