﻿using SimpleFactory.ConcreteProduct;
using SimpleFactory.Product;
using System;

namespace SimpleFactory
{
    public class MobileFactory
    {

        public static IMobile CreateMobile(BrandType brandType)
        {
            switch (brandType)
            {

                case BrandType.Samsung:
                    return new Samsung();

                case BrandType.Xiomi:
                    return new Xiomi();

                case BrandType.Apple: 
                    return new Apple();

                default:
                    throw new Exception("invalid type");
            }

        }

        public static ILaptop CreateLaptop(BrandType brandType)
        {
            switch (brandType)
            {

                case BrandType.Vivobook:
                    return new Vivobook();

                case BrandType.Macbook:
                    return new Macbook();

                default:
                    throw new Exception("invalid type");
            }

        }




    }
}
