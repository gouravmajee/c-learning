﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CodeFirst.Models
{
    public class Products
    {
        [Key]
        public int ProductId { get; set; }

        [Required(ErrorMessage="Product name is required")]
        [Display(Name="Product Name")]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "Price is required")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Qaunttity is required")]
        public int Qty { get; set; }
        public string Remarks { get; set; } 

    }

    public class EFCodeFirstEntities : DbContext
    {
        public DbSet<Products> products{ get; set; }

    }
}