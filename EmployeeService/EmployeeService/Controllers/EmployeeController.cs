﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EmployeeDataAccess;

namespace EmployeeService.Controllers
{
    public class EmployeeController : ApiController
    {
        public HttpResponseMessage Get(string gender="All")
        {
            using (EmployeeDBSEntities entities = new EmployeeDBSEntities())
            {
                switch (gender.ToLower())
                {
                    case "all":
                        return Request.CreateResponse(HttpStatusCode.OK, entities.Employees.ToList());
                    case "male":
                        return Request.CreateResponse(HttpStatusCode.OK, entities.Employees.Where(e=>e.Gender.ToLower()=="male").ToList());
                    case "female":
                        return Request.CreateResponse(HttpStatusCode.OK, entities.Employees.Where(e => e.Gender.ToLower() == "female").ToList());
                    default:
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "value shoudl be make femlale or all");
                }
               //    return entities.Employees.ToList();
            }
        }
        //public IEnumerable<Employee> Get(string gender = 'All')
        //{
        //    using (EmployeeDBSEntities entities = new EmployeeDBSEntities())
        //    {
        //        return entities.Employees.ToList();
        //    }
        //}
        public HttpResponseMessage Get(int id)
        {
            using (EmployeeDBSEntities entities = new EmployeeDBSEntities())
            {

                var entity = entities.Employees.FirstOrDefault(e => e.ID == id);
                if (entities != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Succesfully created");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "employee with id " +  id + " not found ");
                }
            }

        }

        public HttpResponseMessage  Post([FromBody] Employee employee)
        {
            try
            {
                using (EmployeeDBSEntities entities = new EmployeeDBSEntities())
                {
                    entities.Employees.Add(employee);
                    entities.SaveChanges();
                    var message = Request.CreateResponse(HttpStatusCode.Created, employee);
                    message.Headers.Location = new Uri(Request.RequestUri + employee.ID.ToString());
                    return message;
                 }
            }
            catch(Exception ex)
            {
               return  Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
           
        }

        public HttpResponseMessage Put([FromBody] Employee employee,int id)
        {
            try
            {
                using (EmployeeDBSEntities entities = new EmployeeDBSEntities())
                {
                    var updateee = entities.Employees.FirstOrDefault(e => e.ID == id);

                    if (updateee != null)
                    {

                        updateee.FirstName = employee.FirstName;
                        updateee.LastName = employee.LastName;
                        updateee.Gender = employee.Gender;
                        updateee.Salary = employee.Salary;

                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, "Succesfully updated");
                    }
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "employee with id " + id + " not found ");


                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }



        public HttpResponseMessage Delete(int id)
        {
            try
            {
                using (EmployeeDBSEntities entities = new EmployeeDBSEntities())
                {
                    entities.Employees.Remove(entities.Employees.FirstOrDefault(x => x.ID == id));
                    entities.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Succesfully deleetdd");
                    //var itemtoremove = entities.Employees.Where(item => item.ID == 1).First();
                    //if (itemtoremove != null) {
                    //    entities.Employees.Remove(itemtoremove);


                    //}
                    //else{
                    //    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "employee with id " + id + " not found for deletion");
                    //}
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


    }
}
