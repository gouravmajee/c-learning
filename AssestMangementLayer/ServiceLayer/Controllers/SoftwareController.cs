﻿using BusinessLayer;
using BusinessLayer.Interface;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiceLayer.Controllers
{
    public class SoftwareController : Controller
    {
        private readonly ISoftwareBL _isoftwarebl;
        public SoftwareController(ISoftwareBL _isoftwarebl)
        {
            this._isoftwarebl = _isoftwarebl;
        }
        // GET: Book
        public ActionResult Index()
        {
            return View(_isoftwarebl.GetAllSoftware().ToList());

        }

        // GET: Books/Details
        public ActionResult Details(int id)
        {
            return View(_isoftwarebl.GetSoftware(id));
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "SoftwareName,CompanyName,LaunchDate")] Software software)
        {
            if (ModelState.IsValid)
            {
                _isoftwarebl.AddSoftware(software);
                return RedirectToAction("Index");
            }
            return View(software);
        }
        // GET: Books/Edit
        public ActionResult Edit(int id)
        {
            Software software = _isoftwarebl.GetSoftware(id);
            return View(software);
        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "SerialNo,SoftwareName,CompanyName,LaunchDate")] Software software)
        {
            if (ModelState.IsValid)
            {
                _isoftwarebl.EditSoftware(software);
                return RedirectToAction("Index");
            }
            return View(software);

        }
        // GET: Books/Delete/5
        public ActionResult Delete(int id)
        {
            Software software = _isoftwarebl.GetSoftware(id);
            return View(software);
        }
        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            _isoftwarebl.DeleteSoftware(id);
            return RedirectToAction("Index");
        }

        public ActionResult Search(String SearchBy, String Search)
        {
            if (SearchBy == "SoftwareName")
            {
                IEnumerable<Software> software = _isoftwarebl.Search(SearchBy, Search);
                return View("Index", software);
            }
            else if (SearchBy == "CompanyName")
            {
                IEnumerable<Software> software = _isoftwarebl.Search(SearchBy, Search);
                return View("Index", software);
            }
            return RedirectToAction("Index");
        }
    }
}