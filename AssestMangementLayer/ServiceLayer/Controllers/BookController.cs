﻿using BusinessLayer.Interface;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiceLayer.Controllers
{
    public class BookController : Controller
    {
        int num = 5;
        private readonly IBookBL _ibookbl;

        //public BookController()
        //{
        //   _ibookbl=null;
        //}
        //public BookController() : this(new BookBL)
        //{
        //}

        public BookController(IBookBL _ibookbl)
        {
            this._ibookbl = _ibookbl;
        }
        // GET: Book
        public ActionResult Index()
        {
            return View(_ibookbl.GetAllBooks().ToList());

        }

        // GET: Books/Details
        public ActionResult Details(int id)
        {
            return View(_ibookbl.GetBook(id));
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "BookName,AuthorName,PublishDate")] Book book)
        {
            if (ModelState.IsValid)
            {
                _ibookbl.AddBook(book);
                return RedirectToAction("Index");
            }
            return View(book);
        }
        // GET: Books/Edit
        public ActionResult Edit(int id)
        {
            Book book = _ibookbl.GetBook(id);
            return View(book);
        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "SerialNo,BookName,AuthorName,PublishDate")] Book book)
        {
            if (ModelState.IsValid)
            {
                _ibookbl.EditBook(book);
                return RedirectToAction("Index");
            }
            return View(book);

        }
        // GET: Books/Delete/5
        public ActionResult Delete(int id)
        {
            Book book = _ibookbl.GetBook(id);
            return View(book);
        }
        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            _ibookbl.DeleteBook(id);
            return RedirectToAction("Index");
        }

        public ActionResult Search(String SearchBy, String Search)
        {
            if (SearchBy == "BookName")
            {
                IEnumerable<Book> book = _ibookbl.Search(SearchBy, Search);
                return View("Index", book);
            }
            else if (SearchBy == "AuthorName")
            {
                IEnumerable<Book> book = _ibookbl.Search(SearchBy, Search);
                return View("Index", book);
            }
            return RedirectToAction("Index");
        }



    }
}