﻿using BusinessLayer;
using BusinessLayer.Interface;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiceLayer.Controllers
{
    public class HardwareController : Controller
    {
        private readonly IHardwareBL _ihardwarebl;
        // public HardwareController(IHardwareBL _ihardwarebl)
        //{
        //    this._ihardwarebl = _ihardwarebl;
        //}
        public HardwareController(IHardwareBL _ihardwarebl)
        {
            this._ihardwarebl = _ihardwarebl;
        }
        // GET: Book
        public ActionResult Index()
        {
            return View(_ihardwarebl.GetAllHardware().ToList());

        }

        // GET: Books/Details
        public ActionResult Details(int id)
        {
            return View(_ihardwarebl.GetHardware(id));
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "HardwareName,CompanyName,LaunchDate")] Hardware hardware)
        {
            if (ModelState.IsValid)
            {
                _ihardwarebl.AddHardware(hardware);
                return RedirectToAction("Index");
            }
            return View(hardware);
        }
        // GET: Books/Edit
        public ActionResult Edit(int id)
        {
            Hardware hardware = _ihardwarebl.GetHardware(id);
            return View(hardware);
        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "SerialNo,HardwareName,CompanyName,LaunchDate")] Hardware hardware)
        {
            if (ModelState.IsValid)
            {
                _ihardwarebl.EditHardware(hardware);
                return RedirectToAction("Index");
            }
            return View(hardware);

        }
        // GET: Books/Delete/5
        public ActionResult Delete(int id)
        {
            Hardware hardware = _ihardwarebl.GetHardware(id);
            return View(hardware);
        }
        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            _ihardwarebl.DeleteHardware(id);
            return RedirectToAction("Index");
        }

        public ActionResult Search(String SearchBy, String Search)
        {
            if (SearchBy == "HardwareName")
            {
                IEnumerable<Hardware> hardware = _ihardwarebl.Search(SearchBy, Search);
                return View("Index", hardware);
            }
            else if (SearchBy == "CompanyName")
            {
                IEnumerable<Hardware> hardware = _ihardwarebl.Search(SearchBy, Search);
                return View("Index", hardware);
            }
            return RedirectToAction("Index");
        }

    }
}