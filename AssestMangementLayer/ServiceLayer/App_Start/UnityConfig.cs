using System.Web.Mvc;
using Unity;
using Unity.Mvc5;
using DataAccessLayer;
using BusinessLayer;
using DataAccessLayer.Interfaces;
using BusinessLayer.Interface;


namespace ServiceLayer
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IBookDAL, BookDAL>();
            container.RegisterType<IBookBL, BookBL>();
            container.RegisterType<IHardwareDAL, HardwareDAL>();
            container.RegisterType<IHardwareBL, HardwareBL>();
            container.RegisterType<ISoftwareDAL, SoftwareDAL>();
            container.RegisterType<ISoftwareBL, SoftwareBL>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}