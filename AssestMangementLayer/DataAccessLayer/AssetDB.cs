﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class AssetDB : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Software> Softwares { get; set; }
        public DbSet<Hardware> Hardwares { get; set; }
    }
}
