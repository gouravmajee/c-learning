﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer
{
    public class  SoftwareDAL : ISoftwareDAL
    {
        private readonly AssetDB softwareDB = new AssetDB();
        public void AddSoftware(Software Software)
        {
            softwareDB.Softwares.Add(Software);
            softwareDB.SaveChanges();
        }

        public void DeleteSoftware(int id)
        {
            Software software = softwareDB.Softwares.Find(id);
            softwareDB.Softwares.Remove(software);
            softwareDB.SaveChanges();
        }

        public void EditSoftware(Software software)
        {
            softwareDB.Entry(software).State = EntityState.Modified;
            softwareDB.SaveChanges();
        }

        public IEnumerable<Software> GetAllSoftware()
        {
            return softwareDB.Softwares.ToList();
        }

        public Software GetSoftware(int Id)
        {
            return softwareDB.Softwares.Find(Id);
        }

        public IEnumerable<Software> Search(string SearchBy, string Search)
        {
            if (SearchBy == "SoftwareName")
            {
                var result = softwareDB.Softwares.Where(x => x.SoftwareName.StartsWith(Search)).ToList();
                return result;
            }
            else if (SearchBy == "CompanyName")
            {
                var result = softwareDB.Softwares.Where(x => x.CompanyName.StartsWith(Search)).ToList();
                return result;

            }
            else
            {
                IEnumerable<Software> software = null;
                return software;
            }
        }
    }
}
