﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class Hardware
    {
        [Key]
        public int SerialNo { get; set; }
        [Required(ErrorMessage = "Required")]


        public string HardwareName { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression("^[a-zA-Z/s]*$", ErrorMessage = "Only Alphabets are allowed.")]

        public string CompanyName { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime LaunchDate { get; set; }
    }
}
