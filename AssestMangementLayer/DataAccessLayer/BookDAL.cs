﻿using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
  
       public class BookDAL : IBookDAL
        {
            private AssetDB bookDB;

            public BookDAL(AssetDB bookDB)
            {
                this.bookDB = bookDB;
            }

            public void AddBook(Book Book)
            {

                bookDB.Books.Add(Book);
                bookDB.SaveChanges();
            }

            public void DeleteBook(int id)
            {
            Book book = bookDB.Books.Find(id);
                bookDB.Books.Remove(book);
                bookDB.SaveChanges();
            }

            public void EditBook(Book Book)
            {

                bookDB.Entry(Book).State = EntityState.Modified;
                bookDB.SaveChanges();
            }

            public IEnumerable<Book> GetAllBooks()
            {
                return bookDB.Books.ToList();
            }

            public Book GetBook(int Id)
            {
                return bookDB.Books.Find(Id);
            }

            public IEnumerable<Book> Search(string SearchBy, string Search)
            {

                if (SearchBy == "BookName")
                {
                    var result = bookDB.Books.Where(x => x.BookName.StartsWith(Search)).ToList();
                    return result;
                }
                else if (SearchBy == "AuthorName")
                {
                    var result = bookDB.Books.Where(x => x.AuthorName.StartsWith(Search)).ToList();
                    return result;

                }
                else
                {
                    IEnumerable<Book> book = null;
                    return book;
                }
            }

        }
    }