﻿using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class HardwareDAL : IHardwareDAL
    {

        private readonly AssetDB hardwareDB = new AssetDB();
      
        public void AddHardware(Hardware Hardware)
        {
            hardwareDB.Hardwares.Add(Hardware);
            hardwareDB.SaveChanges();
        }

        public void DeleteHardware(int id)
        {
            Hardware hardware = hardwareDB.Hardwares.Find(id);
            hardwareDB.Hardwares.Remove(hardware);
            hardwareDB.SaveChanges();
        }

        public void EditHardware(Hardware Hardware)
        {
            hardwareDB.Entry(Hardware).State = EntityState.Modified;
            hardwareDB.SaveChanges();
        }

        public IEnumerable<Hardware> GetAllHardware()
        {
            return hardwareDB.Hardwares.ToList();
        }

        public Hardware GetHardware(int Id)
        {
            return hardwareDB.Hardwares.Find(Id);
        }

        public IEnumerable<Hardware> Search(string SearchBy, string Search)
        {
            if (SearchBy == "HardwareName")
            {
                var result = hardwareDB.Hardwares.Where(x => x.HardwareName.StartsWith(Search)).ToList();
                return result;
            }
            else if (SearchBy == "CompanyName")
            {
                var result = hardwareDB.Hardwares.Where(x => x.CompanyName.StartsWith(Search)).ToList();
                return result;

            }
            else
            {
                IEnumerable<Hardware> hardware = null;
                return hardware;
            }
        }
    }
}

