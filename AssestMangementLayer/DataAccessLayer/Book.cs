﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class Book
    {
        [Key]
        public int SerialNo { get; set; }
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Book Name")]
        public string BookName { get; set; }
        [Required(ErrorMessage = "Required")]
        [RegularExpression("^[a-zA-Z/s]*$", ErrorMessage = "Only Alphabets are allowed.")]
        [Display(Name = "Author Name")]
        public string AuthorName { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Publish Date")]
        public System.DateTime PublishDate { get; set; }
    }
}
