﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firstmigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        SerialNo = c.Int(nullable: false, identity: true),
                        BookName = c.String(nullable: false),
                        AuthorName = c.String(nullable: false),
                        PublishDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SerialNo);
            
            CreateTable(
                "dbo.Hardwares",
                c => new
                    {
                        SerialNo = c.Int(nullable: false, identity: true),
                        HardwareName = c.String(nullable: false),
                        CompanyName = c.String(nullable: false),
                        LaunchDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SerialNo);
            
            CreateTable(
                "dbo.Softwares",
                c => new
                    {
                        SerialNo = c.Int(nullable: false, identity: true),
                        SoftwareName = c.String(nullable: false),
                        CompanyName = c.String(nullable: false),
                        LaunchDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SerialNo);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Softwares");
            DropTable("dbo.Hardwares");
            DropTable("dbo.Books");
        }
    }
}
