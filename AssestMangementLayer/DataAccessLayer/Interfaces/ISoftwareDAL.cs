﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface ISoftwareDAL
    {
        void AddSoftware(Software Software);

        Software GetSoftware(int Id);

        void DeleteSoftware(int id);

        void EditSoftware(Software Software);

        IEnumerable<Software> GetAllSoftware();

        IEnumerable<Software> Search(String Quary, String value);
    }
}
