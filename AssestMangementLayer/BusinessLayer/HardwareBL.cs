﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Interface;
using DataAccessLayer;
using DataAccessLayer.Interfaces;
using System.Collections.Generic;

namespace BusinessLayer
{
    public class HardwareBL : IHardwareBL
    {
        private readonly IHardwareDAL _ihardwaredl;


        public HardwareBL(IHardwareDAL _ihardwaredl)
        {
            this._ihardwaredl = _ihardwaredl;
        }
        public void AddHardware(Hardware Hardware)
        {
            _ihardwaredl.AddHardware(Hardware);
        }

        public void DeleteHardware(int id)
        {
            _ihardwaredl.DeleteHardware(id);
        }

        public void EditHardware(Hardware Hardware)
        {
            _ihardwaredl.EditHardware(Hardware);
        }

        public IEnumerable<Hardware> GetAllHardware()
        {
            return _ihardwaredl.GetAllHardware();
        }

        public Hardware GetHardware(int Id)
        {
            return _ihardwaredl.GetHardware(Id);
        }

        public IEnumerable<Hardware> Search(string Quary, string value)
        {
            return _ihardwaredl.Search(Quary, value);
        }
    }
}
