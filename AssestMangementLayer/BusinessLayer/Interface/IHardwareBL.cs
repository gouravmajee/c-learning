﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
namespace BusinessLayer.Interface
{
    public interface IHardwareBL
    {
        void AddHardware(Hardware Hardware);
        Hardware GetHardware(int Id);

        void DeleteHardware(int id);


        void EditHardware(Hardware Hardware);

        IEnumerable<Hardware> GetAllHardware();

        IEnumerable<Hardware> Search(String Quary, String value);
    }
}
