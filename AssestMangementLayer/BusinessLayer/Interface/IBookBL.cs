﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;


namespace BusinessLayer.Interface
{
    public interface IBookBL
    {
        void AddBook(Book book);
        Book GetBook(int Id);
        void DeleteBook(int id);

        void EditBook(Book Book);

        IEnumerable<Book> GetAllBooks();

        IEnumerable<Book> Search(String Quary, String value);


    }
}
