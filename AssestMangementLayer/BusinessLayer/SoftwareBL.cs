﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Interface;
using DataAccessLayer;
using DataAccessLayer.Interfaces;
using System.Collections.Generic;

namespace BusinessLayer
{
    public class SoftwareBL : ISoftwareBL
    {
        private readonly ISoftwareDAL _isoftwaredl;


        public SoftwareBL(ISoftwareDAL _isoftwaredl)
        {
            this._isoftwaredl = _isoftwaredl;
        }
        public void AddSoftware(Software Software)
        {
            _isoftwaredl.AddSoftware(Software);
        }

        public void DeleteSoftware(int id)
        {
            _isoftwaredl.DeleteSoftware(id);
        }

        public void EditSoftware(Software Software)
        {
            _isoftwaredl.EditSoftware(Software);
        }

        public IEnumerable<Software> GetAllSoftware()
        {
            return _isoftwaredl.GetAllSoftware();
        }

        public Software GetSoftware(int Id)
        {
            return _isoftwaredl.GetSoftware(Id);
        }

        public IEnumerable<Software> Search(string Quary, string value)
        {
            return _isoftwaredl.Search(Quary, value);
        }
    }
}