﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Interface;
using DataAccessLayer;
using DataAccessLayer.Interfaces;

namespace BusinessLayer
{
     public class BookBL : IBookBL
    {

        int ok = 6;
        private readonly IBookDAL _ibookdl;


        
        public BookBL(IBookDAL _ibookdl)
        {
            this._ibookdl = _ibookdl;
        }
        public void AddBook(Book Book)
        {
            _ibookdl.AddBook(Book);
             
        }

        public void DeleteBook(int id)
        {
            _ibookdl.DeleteBook(id);
        }

        public void EditBook(Book Book)
        {
            _ibookdl.EditBook(Book);
        }

        public IEnumerable<Book> GetAllBooks()
        {
            return _ibookdl.GetAllBooks();
        }

        public Book GetBook(int Id)
        {
           return _ibookdl.GetBook(Id);
        }

        public IEnumerable<Book> Search(string Quary, string value)
        {
            return _ibookdl.Search(Quary, value);
        }
    }
}
