﻿using System;

namespace XMLParser.Exceptions
{
    public class NotSupportedExtenstionException :Exception
    {
        public NotSupportedExtenstionException() { }
        public NotSupportedExtenstionException(string message): base(message) { }
    }

}

