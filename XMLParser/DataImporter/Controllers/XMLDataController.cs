﻿using DataImpoterService.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Xml;

namespace XMLParser.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class XMLDataController : ControllerBase
    {

        private readonly IXMLParserService _XMLParserService;

        public XMLDataController(IXMLParserService XMLParserService) {
            _XMLParserService = XMLParserService;
        }

        public void XMlParserHelper(XmlDocument Doc, string tagname)
        {
            XmlNodeList element = Doc.GetElementsByTagName(tagname);
            for (int i = 0; i < element.Count; i++)
            {
                Console.WriteLine(tagname + " ---> " + element[i].InnerXml);
            }
        }
        [Route("SumValues")]
        public string Sum()
        {
            return "Ok(result)";
        }



        [HttpGet]
        [Route("XMLTOJSON")]
        public string XmlParse()
        {
            XmlDocument myXml = new XmlDocument();
            myXml.Load(@"..\fakeData.xml");

            foreach (XmlNode node in myXml.DocumentElement.ChildNodes)
            {
                Console.WriteLine(node + " is " + node.InnerText);
            }

            var data = new List<string>() {
                "d:Material",
                "d:MfgCo2" ,
                "d:EnergyUseCo2",
                "d:TransportCo2" ,
                "d:PaperCo2",
                "d:ConsumablesCo2",
                "d:EolCo2"
                };

            foreach (var item in data)
            {
                XMlParserHelper(myXml, item);
            }

            return myXml.InnerXml;

        }




            [HttpGet]
        public IEnumerable<IEnumerable<string>> Get()
        {
            return _XMLParserService.Get();

        }

        [HttpPost]
        public void GetData( )
        {
          _XMLParserService.Post();
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

    }
}

