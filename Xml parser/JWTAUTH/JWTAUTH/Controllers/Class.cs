﻿//< feed xmlns = 'http://www.w3.org/2005/Atom' xmlns: m = 'http://schemas.microsoft.com/ado/2007/08/dataservices/metadata' xmlns: d = 'http://schemas.microsoft.com/ado/2007/08/dataservices' xml: base = 'https://hpim5qas01.corp.hpicloud.net:44300/sap/opu/odata/sap/ZPLM_SUSTAIN_CO2_DATA/' >
//          < id > https://hpim5qas01.corp.hpicloud.net:44300/sap/opu/odata/sap/ZPLM_SUSTAIN_CO2_DATA/ET_SUSTAINSET </id>
//< title type = 'text' > ET_SUSTAINSET </ title >
// < updated > 2022 - 04 - 04T17: 56:22Z </ updated >
//         < author >
//         < name />
//         </ author >
//         < link href = 'ET_SUSTAINSET' rel = 'self' title = 'ET_SUSTAINSET' />
//              < entry >
//              < id > https://hpim5qas01.corp.hpicloud.net:44300/sap/opu/odata/sap/ZPLM_SUSTAIN_CO2_DATA/ET_SUSTAINSET(Country='FR',Material='2T1M2A',ChTimeStamp='20220321180430')</id>
//< title type = 'text' > ET_SUSTAINSET(Country = 'FR', Material = '2T1M2A', ChTimeStamp = '20220321180430') </ title >
// < updated > 2022 - 04 - 04T17: 56:22Z </ updated >
//         < category term = 'ZPLM_SUSTAIN_CO2_DATA.ET_SUSTAIN' scheme = 'http://schemas.microsoft.com/ado/2007/08/dataservices/scheme' />
//            < link href = 'ET_SUSTAINSET(Country='FR',Material='2T1M2A',ChTimeStamp='20220321180430')' rel = 'self' title = 'ET_SUSTAIN' />
//                 < content type = 'application/xml' >
//                  < m:properties xmlns:m = 'http://schemas.microsoft.com/ado/2007/08/dataservices/metadata' xmlns: d = 'http://schemas.microsoft.com/ado/2007/08/dataservices' >
//                      < d:Country > FR </ d:Country >
//                           < d:Material > 2T1M2A </ d:Material >
//                                < d:MfgCo2 > 34.230 </ d:MfgCo2 >
//                                     < d:EnergyUseCo2 > 22.900 </ d:EnergyUseCo2 >
//                                          < d:TransportCo2 > 42.420 </ d:TransportCo2 >
//                                               < d:PaperCo2 > 43.430 </ d:PaperCo2 >
//                                                    < d:ConsumablesCo2 > 53.300 </ d:ConsumablesCo2 >
//                                                         < d:EolCo2 > 8.000 </ d:EolCo2 >
//                                                              < d:TotalCo2 > 3.000 </ d:TotalCo2 >
//                                                                   < d:RcpPercentage > 12 </ d:RcpPercentage >
//                                                                        < d:EnergyStar > 0.000 </ d:EnergyStar >
//                                                                             < d:EnergyCost > 0.000 </ d:EnergyCost >
//                                                                                  < d:Currency />
//                                                                                   < d:CreatedBy > APIUSER </ d:CreatedBy >
//                                                                                        < d:ChangedBy > APIUSER </ d:ChangedBy >
//                                                                                             < d:CreTimeStamp > 20220321180430 </ d:CreTimeStamp >
//                                                                                                  < d:ChTimeStamp > 20220321180430 </ d:ChTimeStamp >
//                                                                                                       < d:MatCreatedDate />
//                                                                                                        < d:MatModifDate />
//                                                                                                         < d:MessageType />
//                                                                                                          < d:MessageText />
//                                                                                                           </ m:properties >
//                                                                                                            </ content >
//                                                                                                            </ entry >
//                                                                                                            </ feed >