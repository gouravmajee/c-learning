﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Xml.Linq;
using System;
using System.Xml;
using System.Linq;

namespace JWTAUTH.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArithmaticController : ControllerBase
    {
        public static  double Divide(double number1, double number2)
        {
            if(number1!=0)
            return (number1 / number2);
            else
                return 0;
        }

        [Authorize]
        [HttpPost]
        [Route("SumValues")]
        public IActionResult Sum([FromQuery(Name = "Value1")] int value1, [FromQuery(Name = "Value2")] int value2)
        {
            var result = value1 + value2;
            return Ok(result);
        }


        [HttpGet]
        [Route("XMLTOJSON")]
        public string XmlParse()
        {

            XmlDocument myXml = new XmlDocument();
  

            myXml.Load(@"..\fakeData.xml");

            foreach (XmlNode node in myXml.DocumentElement.ChildNodes)
            {
                Console.WriteLine(node + " is " + node.InnerText);
            }

            //var items = myXml.DocumentElement.SelectNodes("/feed").Cast<XmlElement>().ToList();
            //foreach (var item in items)
            //{
            //    Console.WriteLine(item);  
            //}

            //create function for this operations
            //getElementValue(tagName)
            //getElementValue(tagName)


            XmlNodeList elemList2 = myXml.GetElementsByTagName("d:Material");
            for (int i = 0; i < elemList2.Count; i++)
            {
                Console.WriteLine("Material --> "+ elemList2[i].InnerXml);
            }

            XmlNodeList elemList3 = myXml.GetElementsByTagName("d:MfgCo2");
            for (int i = 0; i < elemList3.Count; i++)
            {
                Console.WriteLine("MfgCo2 --> "+elemList3[i].InnerXml);
            }
            XmlNodeList elemList4 = myXml.GetElementsByTagName("d:EnergyUseCo2");
            for (int i = 0; i < elemList4.Count; i++)
            {
                Console.WriteLine("EnergyUseCo2 --> "+elemList4[i].InnerXml);
            }
            XmlNodeList elemList5 = myXml.GetElementsByTagName("d:TransportCo2");
            for (int i = 0; i < elemList5.Count; i++)
            {
                Console.WriteLine("TransportCo2 --> " +elemList5[i].InnerXml);
            }
            XmlNodeList elemList6 = myXml.GetElementsByTagName("d:PaperCo2");
            for (int i = 0; i < elemList6.Count; i++)
            {
                Console.WriteLine(" PaperCo2--> " +elemList6[i].InnerXml);
            }

            XmlNodeList elemList7 = myXml.GetElementsByTagName("d:ConsumablesCo2");
            for (int i = 0; i < elemList7.Count; i++)
            {
                Console.WriteLine("ConsumablesCo2 --> "+elemList7[i].InnerXml);
            }
            XmlNodeList elemList = myXml.GetElementsByTagName("d:EolCo2");
            for (int i = 0; i < elemList.Count; i++)
            {
                Console.WriteLine("EolCo2 --> " +elemList[i].InnerXml);
            }



            // }
            //XmlNode node = myXml.SelectSingleNode("/feed/updated");
            //XmlNode node = null;
            //if (myXml.SelectSingleNode("/feed/updated").InnerText != null)
            //{
            //    node = myXml.SelectSingleNode("/feed/updated");

            //}

            //string text = node.InnerText;


            return myXml.OuterXml;    

        }
 

    }
}
//System.Xml.XmlDocument myXml = new System.Xml.XmlDocument();
//myXml.Load(@"..\fakeData.xml");

//return myXml.OuterXml;





//var filePath = "C:\\Users\\gourav.majee\\Desktop\\JWTAUTH\\JWTAUTH\\Testfiles\\fakeData.xml";
//XmlDocument xmlDocument = new XmlDocument();

//using (var reader = new StreamReader(FakeData))
//{

//}
//xmlDocument.Load(filePath); //load the xml file

//  string jsonText = JsonConvert.SerializeXmlNode(xmlDocument);
//string payload = "Something";

//return Ok(payload).ForceResultAsXml();