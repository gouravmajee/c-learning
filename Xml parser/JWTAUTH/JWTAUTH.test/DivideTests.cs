using JWTAUTH.Controllers;
using System;
using Xunit;

namespace JWTAUTH.test
{
    public class DivideTests
    {
        [Theory]
        [InlineData(100,10,10)]
        [InlineData(100, 5, 20)]
        [InlineData(int.MaxValue, int.MaxValue, 1)]
        [InlineData(int.MinValue, int.MinValue, 1)]

      //  [InlineData(int.MaxValue, 0, 0)]

        [InlineData(int.MaxValue, 1, int.MaxValue)]
        [InlineData(0, -1, 0)]
        [InlineData(0, 0, 0)]
        [InlineData(0, 1, 0)]







        public void WrongAnswer(int a,int b,int c)
        {
            var num1 = a;
            var num2 = b;
            var expectedValue = c; //Rounded value  
     
            var div = ArithmaticController.Divide(num1, num2);
            //Assert  
            Assert.Equal(expectedValue, div);
        }



        [Theory]
      //  [InlineData(100, -10)]
        [InlineData(100, 20)]
        [InlineData(100, 1)]
        public void DivideByNegative(int x ,int y)
        {
            var num1 = x;
            var num2 = y;
            bool expectedValue = true;
            bool Realvalue = false;

            // Act  
            if (ArithmaticController.Divide(num1, num2) > 0)
               Realvalue = true;

            //Assert  
            Assert.Equal(Realvalue , expectedValue);

        }

    }
}
