https://gaia-apim-dev.azure-api.net/api/ps/filter/brands

{
    "result": {
        "brandsList": [
            "Ace",
            "Acer",
            "acer, ConceptD"
        ]
    },
    "status": {
        "code": 200,
        "message": null
    }
}

------------------------------------------------------------------------------------------------------------

https://gaia-apim-dev.azure-api.net/api/ps/filter/pcproducts
{
    "result": {
        "filterList": [
            "Display resolution (pixels)",
            "Memory size",
            "Internal Storage",
            "Graphics (integrated)",
            "Processor generation",
            "Processor family",
            "Form factor",
            "Operating system",
            "Processor core",
            "Display"
        ],
        "marketingList": [
            {
                "marketingCategory": "Workstations",
                "marketingCategoryPmoid": 296719
            },
            {
                "marketingCategory": "Business Laptop PCs",
                "marketingCategoryPmoid": 64295
            },
            {
                "marketingCategory": "Business Desktop PCs",
                "marketingCategoryPmoid": 64287
            }
        ]
    },
    "status": {
        "code": 200,
        "message": null
    }
}

--------------------------------------------------------------------------------------------

https://gaia-apim-dev.azure-api.net/api/getlocation

{
    "result": [
        {
            "countryName": "Australia",
            "countryCode": "au",
            "currency": "US Dollar",
            "capsLocaleId": 38
        },
        {
            "countryName": "Austria",
            "countryCode": "at",
            "currency": "US Dollar",
            "capsLocaleId": 37
        }
    ],
    "status": {
        "code": 200,
        "message": null
    }
}

