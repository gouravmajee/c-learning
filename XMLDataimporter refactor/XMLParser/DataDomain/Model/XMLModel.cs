﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XMLDataDomain.Model
{
    public  class XMLModel
    {
        public string  Material { get; set; }
        public string MfgCo2 { get; set; }
        public string TransportCo2 { get; set; }
        public string PaperCo2 { get; set; }
        public string ConsumablesCo2 { get; set; }
        public string EolCo2 { get; set; }
        public string TotalCo2 { get; set; }
        public string EnergyStar { get; set; }
        public string EnergyCost { get; set; }
        public string Currency { get; set; }

    }

    public class XMLModelList
    {
        public List<XMLModel> ListOfData { get; set; }
    }
}
