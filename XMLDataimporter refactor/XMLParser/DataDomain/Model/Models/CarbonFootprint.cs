﻿using System.Collections.Generic;

namespace XMLDataDomain.model.models
{
    public class CarbonFootprint
    {
        public  List<Transport> transport { get; set; }
        public List<EnergyConsumption> energyConsumptions { get; set; }
        public List<PaperSavings> paper { get; set; }
        public List<RecycledContent> recycledContents { get; set; }
        public List<Packaging> consumables { get; set; }
    }
}
