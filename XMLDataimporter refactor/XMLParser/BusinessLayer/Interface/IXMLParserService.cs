﻿using System.Collections.Generic;

namespace XMLParser.Interface
{
    public interface IXMLParserService  
    {
        IEnumerable<IEnumerable<string>> Get();
        void GetSum();
        public string TakeData();
        public string TakeDatas();
        public string XmlParse();
        public List<object> SingleBatchTemp(int limit, int offset);
        public List<object> SingleBatch(int limit, int offset);
        public List<List<object>> ContinousBatch();
        public List<string> Testing();

    }
}


