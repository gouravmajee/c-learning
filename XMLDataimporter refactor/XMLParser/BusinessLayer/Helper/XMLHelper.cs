﻿using System.Collections.Generic;
using System.Xml;

namespace XMLParserService.Helper
{
    public static class XMLHelper
    {
        public static List<string> wholedata = new List<string>();

        public static List<string> SustainParameters()
        {
            var Data = new List<string>() {
                "d:Material",
                "d:MfgCo2" ,
                "d:EnergyUseCo2",
                "d:TransportCo2" ,
                "d:PaperCo2",
                "d:ConsumablesCo2",
                "d:EolCo2",
                "d:TotalCo2",
                "d:RcpPercentage",
                "d:EnergyStar",
                "d:EnergyCost",
                "d:Currency"
                };
            return Data;
        }

        public static List<object> Batch_maker(List<object> list,int offset,int limit)
        {

            List<object> result = new List<object>();
            for (int l = offset + 1; l <= offset + limit; l++)
            {
                result.Add(list[l]);
            }
            return result;
        }

        //public static string XMlParserHelper(XmlDocument Doc, string tagname)
        //{
        //    string singleValue = "";

        //    XmlNodeList element = Doc.GetElementsByTagName(tagname);

        //    for (int i = 0; i < element.Count; i++)
        //    {
        //        singleValue = element[i].InnerText;
        //    }
        //    singleValue = x + " / " + y;
        //    return singleValue;

        //}


        public static string XMlParserHelper(XmlDocument Doc, string tagname)
        {
            string singleValue="";

            XmlNodeList element = Doc.GetElementsByTagName(tagname);
            string x = Doc.GetElementsByTagName(tagname)[0].InnerText;
            string y = Doc.GetElementsByTagName(tagname)[1].InnerText;


            //for (int i = 0; i < element.Count; i++)
            //{
            //    singleValue = element[i].InnerText;
            //}
            singleValue = x + " / " + y;
            return singleValue;
        }



        public static List<object> generateData()
        {

            var data0 = new
            {
                Material = "A6L86EA",
                MfgCo2 = 79.000,
                TransportCo2 = 79.000,
                PaperCo2 = 79.000,
                ConsumablesCo2 = 79.000,
                EolCo2 = 79.000,
                TotalCo = 79.000,
                EnergyStar = 79.000,
                EnergyCost = 79,
                Currency = 0.000
            };
            var data1 = new
            {
                Material = "BKBGBBPUR6",
                MfgCo2 = 79.000,
                TransportCo2 = 79.000,
                PaperCo2 = 79.000,
                ConsumablesCo2 = 79.000,
                EolCo2 = 79.000,
                TotalCo = 79.000,
                EnergyStar = 79.000,
                EnergyCost = 79,
                Currency = 0.000
            };
            var data2 = new
            {
                Material = "CONUTCGVJ",
                MfgCo2 = 79.000,
                TransportCo2 = 79.000,
                PaperCo2 = 79.000,
                ConsumablesCo2 = 79.000,
                EolCo2 = 79.000,
                TotalCo = 79.000,
                EnergyStar = 79.000,
                EnergyCost = 79,
                Currency = 0.000
            };
            var data3 = new
            {
                Material = "DKBJVJHUVLG",
                MfgCo2 = 79.000,
                TransportCo2 = 79.000,
                PaperCo2 = 79.000,
                ConsumablesCo2 = 79.000,
                EolCo2 = 79.000,
                TotalCo = 79.000,
                EnergyStar = 79.000,
                EnergyCost = 79,
                Currency = 0.000
            };
            var data4 = new
            {
                Material = "EGBLIINHL",
                MfgCo2 = 79.000,
                TransportCo2 = 79.000,
                PaperCo2 = 79.000,
                ConsumablesCo2 = 79.000,
                EolCo2 = 79.000,
                TotalCo = 79.000,
                EnergyStar = 79.000,
                EnergyCost = 79,
                Currency = 0.000
            };
            var data5 = new
            {
                Material = "FUGGBPUR6",
                MfgCo2 = 79.000,
                TransportCo2 = 79.000,
                PaperCo2 = 79.000,
                ConsumablesCo2 = 79.000,
                EolCo2 = 79.000,
                TotalCo = 79.000,
                EnergyStar = 79.000,
                EnergyCost = 79,
                Currency = 0.000
            };
            var data6 = new
            {
                Material = "GUGGBBPUR",
                MfgCo2 = 79.000,
                TransportCo2 = 79.000,
                PaperCo2 = 79.000,
                ConsumablesCo2 = 79.000,
                EolCo2 = 79.000,
                TotalCo = 79.000,
                EnergyStar = 79.000,
                EnergyCost = 79,
                Currency = 0.000
            };
            var data7 = new
            {
                Material = "HBKUGJJBK",
                MfgCo2 = 79.000,
                TransportCo2 = 79.000,
                PaperCo2 = 79.000,
                ConsumablesCo2 = 79.000,
                EolCo2 = 79.000,
                TotalCo = 79.000,
                EnergyStar = 79.000,
                EnergyCost = 79,
                Currency = 0.000
            };
            var data8 = new
            {
                Material = "IUGGBBPUR",
                MfgCo2 = 79.000,
                TransportCo2 = 79.000,
                PaperCo2 = 79.000,
                ConsumablesCo2 = 79.000,
                EolCo2 = 79.000,
                TotalCo = 79.000,
                EnergyStar = 79.000,
                EnergyCost = 79,
                Currency = 0.000
            };
            var data9 = new
            {
                Material = "JUGGBBPUR",
                MfgCo2 = 79.000,
                TransportCo2 = 79.000,
                PaperCo2 = 79.000,
                ConsumablesCo2 = 79.000,
                EolCo2 = 79.000,
                TotalCo = 79.000,
                EnergyStar = 79.000,
                EnergyCost = 79,
                Currency = 0.000
            };
            var data10= new
            {
                Material = "KUGGBBPUR",
                MfgCo2 = 79.000,
                TransportCo2 = 79.000,
                PaperCo2 = 79.000,
                ConsumablesCo2 = 79.000,
                EolCo2 = 79.000,
                TotalCo = 79.000,
                EnergyStar = 79.000,
                EnergyCost = 79,
                Currency = 0.000
            };



            List<object> list = new List<object>();


            for (int i = 0; i < 25; i++)
            {
            list.Add(data0);
            list.Add(data1);
            list.Add(data2);
            list.Add(data3);
            list.Add(data4);
            list.Add(data5);
            list.Add(data6);
            list.Add(data7);
            list.Add(data8);
            list.Add(data9);

            }


         
            return list;

        }

    }
}





