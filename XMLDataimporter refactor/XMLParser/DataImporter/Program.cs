using Serilog;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using XMLParserService;
using XMLParser.Interface;

namespace XMLParser
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                CreateHostBuilder(args).Build().Run();

                MainStartup();
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        // Main function where we make factoruy objects
        public static void MainStartup()
        {
            IXMLParserService file = XMLParserFactory.CreateParser(filetype.Text);
            file.Get();
        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => {
                    webBuilder.UseStartup<Startup>();
                });
    }
}

