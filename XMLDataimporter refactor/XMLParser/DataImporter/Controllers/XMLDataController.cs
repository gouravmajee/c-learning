﻿using XMLParser.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace XMLParser.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class XMLDataController : ControllerBase, IXMLDataController
    {

        private readonly IXMLParserService _xmlparserservice;

        public XMLDataController(IXMLParserService XMLParserService)
        {
            _xmlparserservice = XMLParserService;
        }

        [Route("SumValues")]
        [HttpGet]
        public void GetSum()
        {
           _xmlparserservice.GetSum();
        }

        [Route("OP")]
        [HttpGet]
        public string TakeData()
        {
            return _xmlparserservice.TakeData();
        }

        [Route("jsondata")]
        [HttpGet]
        public IActionResult TakeDatas()
        {
            var response = _xmlparserservice.TakeDatas();
            return Ok(response);
        }

        [HttpGet]
        [Route("XMLTOJSON")]
        public string XmlParse()
        {
            return _xmlparserservice.XmlParse();
        }

        [Route("SingleBatchTemp")]
        public IActionResult SingleBatchTemp(int limit, int offset)
        {
            var result = _xmlparserservice.SingleBatchTemp(limit, offset);
            return Ok(result);
        }

        //single batch and continous batches
        [Route("SingleBatch")]
        public List<object> SingleBatch(int limit, int offset)
        {
            var result = _xmlparserservice.SingleBatch(limit, offset);
            return result;
        }
            
        [Route("ContinousBatch")]
        public List<List<object>> ContinousBatch()
        {
           var batch = _xmlparserservice.ContinousBatch();
           return batch;
        }


        [HttpGet]
        [Route("testing")]
        public List<string> Testing()
        {
            return  _xmlparserservice.Testing();
        }
    }
}

//[Route("SingleBatchTemp")]
//public IActionResult SingleBatchTemp(int limit, int offset)
//{
//    List<object> list = generateData();
//    List<object> result = new List<object>();
//    for (int index = offset + 1; index <= offset + limit; index++)
//    {
//        result.Add(list[index]);
//    }
//    return Ok(result);
//}

//[Route("ContinousBatch")]
//public List<object> ContinousBatch()
//{
//    List<object> list = generateData();
//    const int batchlimit = 5;
//    var batch = new List<object>();
//    for (int i = 1, offset = 1; i <= list.Count; i++, offset += batchlimit)
//    {
//        batch.Add(SingleBatch(2, offset));
//    }
//    return batch;
//}