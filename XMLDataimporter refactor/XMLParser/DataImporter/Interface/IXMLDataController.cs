﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace XMLParser.Controllers
{
    public interface IXMLDataController
    {
        void GetSum();
        string TakeData();
        IActionResult TakeDatas();
        string XmlParse();
        IActionResult SingleBatchTemp(int limit, int offset);
        List<object> SingleBatch(int limit, int offset);
        List<List<object>> ContinousBatch();
        List<string> Testing();
    }
}

