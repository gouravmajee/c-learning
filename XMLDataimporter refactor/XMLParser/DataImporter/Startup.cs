using XMLParserService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using XMLParser.Interface;

namespace XMLParser
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMvc();
            services.AddScoped<IXMLParserService, XMLParserServices>();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

        app.UseRouting();

            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}






//using System.Xml;
//using XMLParser.Interface;
//using Microsoft.AspNetCore.Mvc;
//using System.Collections.Generic;
//using XMLDataDomain.Model;
//using System.Text.Json;
//using static XMLParserService.Helper.XMLHelper;
//using System;
//using System.Linq;

//namespace XMLParser.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class XMLDataController : ControllerBase, IXMLDataController
//    {

//        private readonly IXMLParserService _XMLParserService;

//        public XMLDataController(IXMLParserService XMLParserService)
//        {
//            _XMLParserService = XMLParserService;
//        }

//        [Route("SumValues")]
//        public string Sum()
//        {
//            return "gourav sum valyes";
//        }

//        [Route("OP")]
//        [HttpGet]
//        public string TakeData()
//        {
//            XmlDocument myXml = new XmlDocument();
//            //myXml.Load(@"..\fakeData.xml");
//            myXml.Load(@"..\FakeData2.xml");

//            List<string> ResultData = new List<string>();

//            foreach (var item in SustainParameters())
//            {
//                ResultData.Add(XMlParserHelper(myXml, item));
//            }

//            var datamodel = new List<XMLModel> {
//                new XMLModel {
//                    Material = ResultData[0],
//                    MfgCo2 = ResultData[1],
//                    TransportCo2 = ResultData[2],
//                    PaperCo2 = ResultData[3],
//                    ConsumablesCo2 = ResultData[4],
//                    EolCo2 = ResultData[5],
//                    TotalCo2 = ResultData[6],
//                    EnergyStar = ResultData[7],
//                    EnergyCost = ResultData[8],
//                    Currency = ResultData[9]
//                },
//               };

//            var jsonResponse = JsonSerializer.Serialize(datamodel);
//            return jsonResponse.ToString();
//        }


//        [Route("jsondata")]
//        [HttpGet]
//        public IActionResult TakeDatas()
//        {
//            XmlDocument myXml = new XmlDocument();
//            //myXml.Load(@"..\fakeData.xml");
//            myXml.Load(@"..\FakeData2.xml");

//            List<string> ResultData = new List<string>();

//            foreach (var item in SustainParameters())
//            {
//                ResultData.Add(XMlParserHelper(myXml, item));
//            }
//            var datamodel = new List<XMLModel> {
//                new XMLModel {
//                    Material = ResultData[0],
//                    MfgCo2 = ResultData[1],
//                    TransportCo2 = ResultData[2],
//                    PaperCo2 = ResultData[3],
//                    ConsumablesCo2 = ResultData[4],
//                    EolCo2 = ResultData[5],
//                    TotalCo2 = ResultData[6],
//                    EnergyStar = ResultData[7],
//                    EnergyCost = ResultData[8],
//                    Currency = ResultData[9]
//                },
//               };

//            var jsonResponse = JsonSerializer.Serialize(datamodel);

//            return Ok(jsonResponse);
//        }



//        [HttpGet]
//        [Route("XMLTOJSON")]
//        public string XmlParse()
//        {
//            XmlDocument myXml = new XmlDocument();
//            //myXml.Load(@"..\fakeData.xml");
//            myXml.Load(@"..\FakeData2.xml");

//            //var nsmgr = new XmlNamespaceManager(myXml.NameTable);
//            //nsmgr.AddNamespace("cbc", "urn:xxx"); //for example
//            //nsmgr.AddNamespace("cac", "urn:yyy");

//            //XmlNode ok = myXml.SelectSingleNode("/feed/entry/content/m:properties/d:ChTimeStamp");
//            //Console.WriteLine(ok.InnerText);

//            foreach (var item in SustainParameters())
//            {
//                XMlParserHelper(myXml, item);
//            }
//            return myXml.InnerXml;
//        }




//        [Route("BatchData")]
//        public IActionResult ManyJsonData(int limit, int offset)
//        {

//            List<object> list = generateData();

//            var okkk = Batch_maker(list, offset, limit);

//            List<object> result = new List<object>();
//            for (int l = offset + 1; l <= offset + limit; l++)
//            {
//                result.Add(list[l]);
//            }

//            Console.WriteLine(result);
//            Console.WriteLine("---------");


//            return Ok(result);
//        }



//        [Route("BatchFetcher")]
//        public void BatchFetcher()
//        {
//            List<object> list = generateData();
//            int limit = 5;
//            for (int batch = 0, offset = 0; batch < list.Count; batch += 4, offset += limit)
//            {
//                ManyJsonData(limit, offset);
//            }
//        }
//    }
//}

