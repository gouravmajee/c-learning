﻿using JWTAUTH.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JWTAUTH.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public IActionResult Login([FromForm] AuthenticationRequest authenticationRequest)
        {
            var jwtAuthenticationManager = new JwtAuthenticationManager();
            var authResult = jwtAuthenticationManager.Authenticate(authenticationRequest.UserName, authenticationRequest.Password);
            if (authResult == null)
                return Unauthorized("Email or password is invalid ");
            else
                return Ok(authResult);
        }

        [HttpGet]
        [Route("Home")]
        public string Home()
        {
            return "Welcome to home page";
        }

        [Authorize]
        [HttpGet]
        [Route("Money")]
        public  ContentResult Money()
        {
            return base.Content(
                "<h1>💸💸💸💸💸💸💸💸</h1>", "text/html");
        }
    }
}
