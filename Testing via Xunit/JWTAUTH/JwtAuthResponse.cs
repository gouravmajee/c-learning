﻿using System;

namespace JWTAUTH
{

    [Serializable]
    public class JwtAuthResponse
    {
        public string token { get; set; }    
        public string userName { get; set; }
        public int expires_in { get; set; }
    }
}
