﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace JWTAUTH
{
    public class JwtAuthenticationManager
    {
        public JwtAuthResponse Authenticate(string userName, string password)
        {
            if (userName != Constants.SecretName || password != Constants.SecretPassword)
            {
                return null;
            }
            //{13-04-2022 11:09:34}
            var tokenExpiryTimeStamp = DateTime.Now.AddMinutes(Constants.JWT_token_validity_mins);

            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            //{byte[20]}
            var tokenKey = Encoding.ASCII.GetBytes(Constants.JWT_security_key); 


            var securityTokenDescriptor = new SecurityTokenDescriptor {
                Subject = new ClaimsIdentity(new List<Claim>
                {
                    new Claim("username", userName),
                   // new Claim(ClaimTypes.PrimaryGroupSid, "User Group 01")
                }),
                Expires = DateTime.Now.AddMinutes(Constants.JWT_token_validity_mins),
              SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)
            };

            //{ { "alg":"HS256","typ":"JWT"}.{ "username":"gourav","nbf":1649826975,"exp":1649828774,"iat":1649826975} }
            var securityToken = jwtSecurityTokenHandler.CreateToken(securityTokenDescriptor);
           
            //"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImdvdXJhdiIsInByaW1hcnlncm91cHNpZCI6IlVzZXIgR3JvdXAgMDEiLCJuYmYiOjE2NDk4MjY3MzMsImV4cCI6MTY0OTgyODQ3MSwiaWF0IjoxNjQ5ODI2NzMzfQ.HjPxfWcOSB2WTLDedlsg78bNIyfJHZrzDmk_MrSIE1E"
            var token = jwtSecurityTokenHandler.WriteToken(securityToken);

            return new JwtAuthResponse
            {
                token = token,
                userName = userName,
                expires_in = (int)tokenExpiryTimeStamp.Subtract(DateTime.Now).TotalSeconds
            };
        }
    }
}

//A SecurityTokenHandler = designed for creating and validating Json Web Tokens.
// SecurityTokenDescriptor =\> This is a place holder for all the attributes related to the issued token.
// Subject => Gets or sets the output claims to be included in the issued token.
//claims identiity =>   The claims identity that contains the output claims.
