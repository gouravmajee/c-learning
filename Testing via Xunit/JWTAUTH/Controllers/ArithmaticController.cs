﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JWTAUTH.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArithmaticController : ControllerBase
    {
        public static  double Divide(double number1, double number2)
        {
            if(number1!=0)
            return (number1 / number2);
            else
                return 0;
        }

        [Authorize]
        [HttpPost]
        [Route("SumValues")]
        public IActionResult Sum([FromQuery(Name = "Value1")] int value1, [FromQuery(Name = "Value2")] int value2)
        {
            var result = value1 + value2;
            return Ok(result);
        }



   
    }
}
