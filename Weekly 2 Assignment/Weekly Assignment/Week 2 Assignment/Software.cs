﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_2_Assignment
{
    public class Software: Operations
    {
        List<Software> softwares = new List<Software>();
        public static int series = 0;
        public int serialNo
        {
            get; set;
        }
        public String softwareName
        {
            get; set;
        }
        public String companyName
        {
            get; set;
        }
        public DateTime launchDate
        {
            get; set;
        }
        public void Add()
        {
            Software software = new Software();
            software.serialNo = ++series;
            Console.WriteLine("Enter the Software Name");
            software.softwareName = Console.ReadLine();
            Console.WriteLine("Enter the Company Name");
            software.companyName = Console.ReadLine();
            Console.WriteLine("Enter the Manufacture Date : DD-MM-YY");
            software.launchDate = DateTime.Parse(Console.ReadLine());
            softwares.Add(software);
        }
        public void Display()
        {
            Console.WriteLine(String.Format("\n{0,-52} | {1,-50}| {2,-30} | {3,-5}", "Serial Number", "SoftwareName", "Company", "Launch-Date"));
            foreach (Software software in softwares)
            {
                Console.WriteLine(String.Format("{0,-52} | {1,-50}| {2,-30} | {3,-5}\n", software.serialNo, software.softwareName, software.companyName, software.launchDate));
            }
        }
        public void search()
        {
            Console.WriteLine("Enter the Choice of Search:");
            Console.WriteLine("1:SoftWareName");
            Console.WriteLine("2:Company");
            Console.WriteLine("3:LaunchDate");
            int Entry = Convert.ToInt32(Console.ReadLine());
            switch (Entry)
            {
                case 1:
                    Console.WriteLine("Enter the Software Name");
                    String SoftWareNaam  = Console.ReadLine();
                    Console.WriteLine(String.Format("\n{0,-52} | {1,-50}| {2,-30} | {3,-5}", "Serial Number", "SoftwareName", "Company", "Launch-Date"));
                    foreach (Software software in softwares)
                    {
                        if (software.softwareName == SoftWareNaam.ToLower()|| software.softwareName == SoftWareNaam.ToUpper()|| software.softwareName == SoftWareNaam)
                        {
                            Console.WriteLine(String.Format("{0,-52} | {1,-50}| {2,-30} | {3,-5}\n", software.serialNo, software.softwareName, software.companyName, software.launchDate));
                        }

                    }

                    break;
                case 2:
                    Console.WriteLine("Enter the Company Name");
                    String companyNaam = Console.ReadLine();
                    Console.WriteLine(String.Format("\n{0,-52} | {1,-50}| {2,-30} | {3,-5}", "Serial Number", "SoftwareName", "Company", "Launch-Date"));
                    foreach (Software software in softwares)
                    {
                        if (software.companyName == companyNaam.ToLower()|| software.companyName == companyNaam.ToLower()|| software.companyName == companyNaam)
                        {
                            Console.WriteLine(String.Format("{0,-52} | {1,-50}| {2,-30} | {3,-5}\n", software.serialNo, software.softwareName, software.companyName, software.launchDate));
                        }

                    }
                    break;
                case 3:
                    Console.WriteLine("Enter the LaunchDate");
                    DateTime PublishDate = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine(String.Format("\n{0,-52} | {1,-50}| {2,-30} | {3,-5}", "Serial Number", "SoftwareName", "Company", "Launch-Date"));
                    foreach (Software software in softwares)
                    {
                        if (software.launchDate.Equals(PublishDate))
                        {
                            Console.WriteLine(String.Format("{0,-52} | {1,-50}| {2,-30} | {3,-5}\n", software.serialNo, software.softwareName, software.companyName, software.launchDate));
                        }

                    }
                    break;
                default:
                    Console.WriteLine("Please Enter Valid Type");
                    break;
            }
        }
        public void update()
        {

        }
        public void Delete()
        {

        }
    }
}
