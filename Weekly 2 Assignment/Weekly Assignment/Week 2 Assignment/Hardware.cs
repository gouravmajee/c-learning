﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_2_Assignment
{
    internal class Hardware:Operations
    {
        List<Hardware> hardwares = new List<Hardware>();
        int series = 0;
        public int serialNo
        {
            get;set;
        }
        public String hardWareName
        {
            get;set;
        }
        public String companyName
        {
            get;set;
        }
        public DateTime launchDate
        {
            get;set;
        }
        public void Add()
        {
            Hardware hardware = new Hardware();
            hardware.serialNo = ++series;
            Console.WriteLine("Enter the Hardware Name");
            hardware.hardWareName = Console.ReadLine();
            Console.WriteLine("Enter the Company Name");
            hardware.companyName = Console.ReadLine();
            Console.WriteLine("Enter the Manufacture Date : DD-MM-YY");
            hardware.launchDate = DateTime.Parse(Console.ReadLine());
            hardwares.Add(hardware);
        }
        public void Display()
        {
            Console.WriteLine(String.Format("\n{0,-13} | {1,-13}| {2,-13} | {3,-13}", "Serial Number", "HardwareName", "Company", "Launch-Date"));
            foreach (Hardware hardware in hardwares)
            {
                Console.WriteLine(String.Format("{0,-13} | {1,-13}| {2,-13} | {3,-13}\n", hardware.serialNo, hardware.hardWareName, hardware.companyName, hardware.launchDate));
            }
        }
        public void search()
        {
            Console.WriteLine("Enter the Choice of Search:");
            Console.WriteLine("1:HardWareName");
            Console.WriteLine("2:Company");
            Console.WriteLine("3:LaunchDate");
            int Entry = Convert.ToInt32(Console.ReadLine());
            switch (Entry)
            {
                case 1:
                    Console.WriteLine("Enter the Hardware Name");
                    String HardwareNaam = Console.ReadLine();
                    Console.WriteLine(String.Format("\n{0,-52} | {1,-50}| {2,-30} | {3,-5}", "Serial Number", "HardwareName", "Company", "Launch-Date"));
                    foreach (Hardware hardware in hardwares)
                    {
                        if (hardware.hardWareName == HardwareNaam.ToLower() || hardware.hardWareName == HardwareNaam.ToUpper() || hardware.hardWareName == HardwareNaam)
                        {
                            Console.WriteLine(String.Format("{0,-52} | {1,-50}| {2,-30} | {3,-5}\n", hardware.serialNo, hardware.hardWareName, hardware.companyName, hardware.launchDate));
                        }

                    }

                    break;
                case 2:
                    Console.WriteLine("Enter the Company Name");
                    String CompanyNaam = Console.ReadLine();
                    Console.WriteLine(String.Format("\n{0,-13} | {1,-13}| {2,-13} | {3,-13}", "Serial Number", "HardwareName", "Company", "Launch-Date"));
                    foreach (Hardware hardware in hardwares)
                    {
                        if (hardware.companyName == CompanyNaam.ToLower() || hardware.companyName==CompanyNaam.ToUpper() || hardware.companyName==CompanyNaam)
                        {
                            Console.WriteLine(String.Format("{0,-13} | {1,-13}| {2,-13} | {3,-13}\n", hardware.serialNo, hardware.hardWareName, hardware.companyName, hardware.launchDate));
                        }

                    }
                    break;
                case 3:
                    Console.WriteLine("Enter the Launch Date");
                    DateTime PublishDate = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine(String.Format("\n{0,-13} | {1,-13}| {2,-13} | {3,-13}", "Serial Number", "HardwareName", "Company", "Launch-Date"));
                    foreach (Hardware hardware in hardwares)
                    {
                        if (hardware.launchDate.Equals(PublishDate))
                        {
                            Console.WriteLine(String.Format("{0,-13} | {1,-13}| {2,-13} | {3,-13}\n", hardware.serialNo, hardware.hardWareName, hardware.companyName, hardware.launchDate));
                        }

                    }
                    break;
                default:
                    Console.WriteLine("Please Enter Valid Type");
                    break;
            }
        }
        public void update()
        {

        }
        public void Delete()
        {

        }
    }
}
