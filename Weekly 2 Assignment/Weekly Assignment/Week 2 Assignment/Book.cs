﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_2_Assignment
{
    public class Book:Operations
    {
        List<Book> books = new List<Book>();
        public static int series = 0;
        public int serialNo
        {
            get; set;
        }
        public String bookName
        {
            get; set;
        }
        public String authorName
        {
            get; set;
        }
        public DateTime publishDate
        {
            get; set;
        }
        public Boolean issafe(DateTime Date)
        {
            if(Date==null)
                return false;
            return true;
        }

        public void Add()
        {
            Book book = new Book();
            book.serialNo = ++series;
            Console.WriteLine("Enter the Book Name");
            book.bookName = Console.ReadLine();
            Console.WriteLine("Enter the Author Name");
            book.authorName = Console.ReadLine();
            Console.WriteLine("Enter the Publish Date : DD-MM-YY");
            book.publishDate = DateTime.Parse(Console.ReadLine());
            try
            {
                if(issafe(publishDate))
                {
                    books.Add(book);
                }
            }

            catch(Exception ex)
            {
                Console.WriteLine("Enter Valid Date");
            }
            
        }
        public void Display()
        {
            Console.WriteLine(String.Format("\n{0,-52} | {1,-50}| {2,-30} | {3,-5}", "Serial Number", "Name","Author","Publish-Date"));
            
            foreach (Book boook in books)
            {
                
                Console.WriteLine(String.Format("{0,-52} | {1,-50}| {2,-30} | {3,-5}\n", boook.serialNo, boook.bookName, boook.authorName,boook.publishDate));
                
            }
        }
        public void search()
        {
            Console.WriteLine("Enter the Choice of Search:");
            Console.WriteLine("1:BookName");
            Console.WriteLine("2:AuthorName");
            Console.WriteLine("3:PublishDate");
            int Entry = Convert.ToInt32(Console.ReadLine());
            switch (Entry)
            {
                case 1:
                    Console.WriteLine("Enter the BookName");
                    String bookNaam = Console.ReadLine();
                    Console.WriteLine(String.Format("\n{0,-52} | {1,-50}| {2,-30} | {3,-5}", "Serial Number", "Name", "Author", "Publish-Date"));
                    foreach (Book boook in books)
                    {
                        if (boook.bookName == bookNaam.ToLower()|| boook.bookName == bookNaam.ToUpper()|| boook.bookName == bookNaam)
                        {
                            Console.WriteLine(String.Format("{0,-52} | {1,-50}| {2,-30} | {3,-5}\n", boook.serialNo, boook.bookName, boook.authorName, boook.publishDate));

                        }

                    }

                    break;
                case 2:
                    Console.WriteLine("Enter the AuthorName");
                    String AuthorNaam = Console.ReadLine();
                    Console.WriteLine(String.Format("\n{0,-52} | {1,-50}| {2,-30} | {3,-5}", "Serial Number", "Name", "Author", "Publish-Date"));

                    foreach (Book boook in books)
                    {
                        if (boook.authorName == AuthorNaam.ToLower()|| boook.authorName == AuthorNaam.ToUpper()|| boook.authorName == AuthorNaam)
                        {
                           
                            Console.WriteLine(String.Format("{0,-52} | {1,-50}| {2,-30} | {3,-5}\n", boook.serialNo, boook.bookName, boook.authorName, boook.publishDate));
                        }
                        
                    }
                    break;
                case 3:
                    Console.WriteLine("Enter the PublishDate");
                    DateTime PublishDate = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine(String.Format("\n{0,-52} | {1,-50}| {2,-30} | {3,-5}", "Serial Number", "Name", "Author", "Publish-Date"));
               
                    foreach (Book boook in books)
                    {
                        if (boook.publishDate.Equals(PublishDate))
                        {
                           
                            Console.WriteLine(String.Format("{0,-52} | {1,-50}| {2,-30} | {3,-5}\n", boook.serialNo, boook.bookName, boook.authorName, boook.publishDate));
                        }
                        
                    }
                    break;
                default:Console.WriteLine("Please Enter Valid Type");
                    break;
            }
        }
        
        


        public void update()
        {

        }
        public void Delete()
        {

        }
    }
}
