//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AssetManagementSystem.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Software
    {
        [Range(1, Int32.MaxValue, ErrorMessage = "Sotware id Should Not be Negative")]
        [Required(ErrorMessage = "Hardware type  is required")]
        public int Software_Id { get; set; }


        [Required(ErrorMessage = "Software Name  is required")]
        public string Software_Name { get; set; }


        [Required(ErrorMessage = "Software Author  is required")]

        public string Software_Author { get; set; }
        public System.DateTime Date_Of_Publish { get; set; }
    }
}
