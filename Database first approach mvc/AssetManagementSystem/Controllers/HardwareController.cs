﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetManagementSystem.Models;
using System.Data.Entity;

namespace AssetManagementSystem.Controllers
{
    public class HardwareController : Controller
    {
        AssestsManagementEntities obj = new AssestsManagementEntities();


        [HttpGet]
        public ActionResult Index()
        {
            List<Hardware> hardware= obj.Hardwares.ToList();
            return View(hardware);

        }

        //[HttpGet]
        //public ActionResult info(int id)
        //{
        //    using (AssestsManagementEntities assetdataentities = new AssestsManagementEntities())
        //        return View(assetdataentities.Hardwares.Where(x => x.Model_no == id).FirstOrDefault());
        //}

        public ActionResult search(string SearchBy, string search)
        {
            if (SearchBy == "Hardware_type") {

                var searchavalue = obj.Hardwares.Where(x => x.Hardware_type == search).ToList();
                return View("Index", searchavalue);
            }
            else{
                return RedirectToAction("Index");
            }

        }


        [HttpGet]
        public ActionResult Details(int id)
        {
            AssestsManagementEntities assetdataentities = new AssestsManagementEntities();
                return View(assetdataentities.Hardwares.Where(x => x.Model_no == id).FirstOrDefault());
        }


        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Hardware hardware)
        {
            try
            {
               AssestsManagementEntities assetdataentities = new AssestsManagementEntities();
                    assetdataentities.Hardwares.Add(hardware);
                    assetdataentities.SaveChanges();
                
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            AssestsManagementEntities assetdataentities = new AssestsManagementEntities();
                return View(assetdataentities.Hardwares.Where(x => x.Model_no == id).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Edit(int id, Hardware hardware)
        {
            try
            {
                AssestsManagementEntities temp = new AssestsManagementEntities();
                temp.Entry(hardware).State = EntityState.Modified;
                temp.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
                AssestsManagementEntities temp = new AssestsManagementEntities();
                return View(temp.Hardwares.Where(x => x.Model_no == id).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                    AssestsManagementEntities temp = new AssestsManagementEntities();
                     Hardware hardware = temp.Hardwares.Where(x => x.Model_no== id).FirstOrDefault();
                      temp.Hardwares.Remove(hardware);
                     temp.SaveChanges();     
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}











