﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetManagementSystem.Models;
using System.Data.Entity;
namespace AssetManagementSystem.Controllers
{
    public class SoftwareController : Controller
    {

        AssestsManagementEntities obj = new AssestsManagementEntities();
        [HttpGet]
        public ActionResult Index()
        {
            List<Software> softwares = obj.Softwares.ToList();
            return View(softwares);

        }


        public ActionResult search(string SearchBy, string search)
        {
            if (SearchBy == "Software_Name") {
                var searchavalue = obj.Softwares.Where(x => x.Software_Name == search).ToList();
                return View("Index", searchavalue);
            }
            else{
                return RedirectToAction("Index");
            }
        }


        [HttpGet]
        public ActionResult Details(int id)
        {
            AssestsManagementEntities temp = new AssestsManagementEntities();
                return View(temp.Softwares.Where(x => x.Software_Id == id).FirstOrDefault());
        }

 
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Software software)
        {
            try
            {
                AssestsManagementEntities assetdataentities = new AssestsManagementEntities();
                
                   assetdataentities.Softwares.Add(software);
                    assetdataentities.SaveChanges();    
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            AssestsManagementEntities temp = new AssestsManagementEntities();
                return View(temp.Softwares.Where(x => x.Software_Id == id).FirstOrDefault());

        }

        [HttpPost]
        public ActionResult Edit(int id, Software software)
        {
            try
            {
               AssestsManagementEntities temp = new AssestsManagementEntities();

                temp.Entry(software).State = EntityState.Modified;
                temp.SaveChanges();
                
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            using (AssestsManagementEntities temp = new AssestsManagementEntities())
                return View(temp.Softwares.Where(x => x.Software_Id == id).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                AssestsManagementEntities temp = new AssestsManagementEntities();
                
                    Software software = temp.Softwares.Where(x => x.Software_Id == id).FirstOrDefault();
                    temp.Softwares.Remove(software);
                    temp.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
