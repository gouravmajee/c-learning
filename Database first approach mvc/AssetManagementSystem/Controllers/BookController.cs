﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetManagementSystem.Models;
using System.Data.Entity;

namespace AssetManagementSystem.Controllers
{
    public class BookController : Controller
    {
        AssestsManagementEntities obj1 = new AssestsManagementEntities();
        
        [HttpGet]
        public ActionResult Index(){

            List<Book> books = obj1.Books.ToList();

            return View(books);
        }

 

     
        [HttpGet]
        public ActionResult Details(int id){
            using (AssestsManagementEntities temp = new AssestsManagementEntities())
                return View( temp.Books.Where(x => x.Id == id).FirstOrDefault() );
        }


       
        public ActionResult Create()
        {
            return View();
        }

        
        [HttpPost]
        public ActionResult Create(Book book)
        {
            try
            {
                    AssestsManagementEntities temp = new AssestsManagementEntities();
                    temp.Books.Add(book);
                    temp.SaveChanges();
                
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            AssestsManagementEntities temp = new AssestsManagementEntities();
                return View(temp.Books.Where(x => x.Id == id).FirstOrDefault());

        }

        [HttpPost]
        public ActionResult Edit(int id, Book book)
        {
            try
            {
                AssestsManagementEntities temp = new AssestsManagementEntities();
                
                    temp.Entry(book).State = EntityState.Modified;
                    temp.SaveChanges();
                

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            AssestsManagementEntities temp = new AssestsManagementEntities();
                return View(temp.Books.Where(x => x.Id == id).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                AssestsManagementEntities temp = new AssestsManagementEntities();
                    Book book = temp.Books.Where(x => x.Id == id).FirstOrDefault();
                    temp.Books.Remove(book);
                    temp.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        public ActionResult search(string SearchBy, string search)
        {   
            if (SearchBy == "Name")
            {
                var searchavalue = obj1.Books.Where(x => x.Name == search).ToList();
                return View("Index", searchavalue);
            }
            else if (SearchBy == "Author")
            {
                var searchavalue = obj1.Books.Where(x => x.Author == search).ToList();
                return View("Index", searchavalue);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
    }
}


