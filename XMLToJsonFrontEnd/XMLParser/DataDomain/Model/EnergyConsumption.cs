﻿using System.Collections.Generic;

namespace XMLDataDomain.model
{
    public class EnergyConsumption
    {
        public int kwh { get; set; }
        public int electricityCost { get; set; }
       // public List<EnergyConsumption> energyConsumption { get; set; }

    }
}
