﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XMLDataDomain.Model
{
    public class EstimatedElectricity
    {
        public int qty { get; set; }
        public int kwh { get; set; }
        public int cost { get; set; }
    }
}
