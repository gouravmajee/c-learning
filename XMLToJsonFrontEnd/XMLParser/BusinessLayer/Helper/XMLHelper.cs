﻿using XMLParserService.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Xml;
using XMLDataDomain.Model;

namespace XMLParserService.Helper
{
    public static class XMLHelper
    {
        public static List<string> wholedata = new List<string>();

        public static List<string> SustainParameters()
        {
            var Data = new List<string>() {
                "d:Material",
                "d:MfgCo2" ,
                "d:EnergyUseCo2",
                "d:TransportCo2" ,
                "d:PaperCo2",
                "d:ConsumablesCo2",
                "d:EolCo2",
                "d:TotalCo2",
                "d:RcpPercentage",
                "d:EnergyStar",
                "d:EnergyCost",
                "d:Currency"
                };

            return Data;
        }

        public static string XMlParserHelper(XmlDocument Doc, string tagname)
        {
            string sasta="";
            XmlNodeList element = Doc.GetElementsByTagName(tagname);

            for (int i = 0; i < element.Count; i++)
            {
                ModelAdder(tagname, element[i].InnerXml);
                sasta=element[i].InnerText;
                //var model2 = new List<XMLModel> {
                //new XMLModel {
                //Material ="materill",
                //MfgCo2 ="mccakns",
                //TransportCo2 ="mccakns",
                //PaperCo2="mccakns",
                //ConsumablesCo2 ="mccakns",
                //EolCo2 ="mccakns",
                //TotalCo2 ="mccakns",
                //EnergyStar="mccakns",
                //EnergyCost="mccakns",
                //Currency="mccakns" },
                //};
                // Console.WriteLine(tagname + " ---> " + element[i].InnerXml);
            }
            return sasta;
        }

        public static void JsonExecutor(XmlDocument Doc, string tagname)
        {
            XmlNodeList element = Doc.GetElementsByTagName(tagname);
            for (int i = 0; i < element.Count; i++)
            {
                ModelAdder(tagname, element[i].InnerXml);

                //var model2 = new List<XMLModel> {
                //new XMLModel {
                //Material ="materill",
                //MfgCo2 ="mccakns",
                //TransportCo2 ="mccakns",
                //PaperCo2="mccakns",
                //ConsumablesCo2 ="mccakns",
                //EolCo2 ="mccakns",
                //TotalCo2 ="mccakns",
                //EnergyStar="mccakns",
                //EnergyCost="mccakns",
                //Currency="mccakns" },
                //};
                // Console.WriteLine(tagname + " ---> " + element[i].InnerXml);

            }
        }

        public static void ModelAdder(string tag, string element)
        {
            wholedata.Add(tag + element);
        }

        public static void GetWholeData()
        {
            foreach (var item in wholedata)
            {
                Console.WriteLine("wholedata is " + item);

            }
        }

    }
}



