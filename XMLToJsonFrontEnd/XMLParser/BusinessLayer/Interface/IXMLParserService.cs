﻿using System.Collections.Generic;

namespace XMLParserService.Interface
{
    public interface IXMLParserService  
    {
        IEnumerable<IEnumerable<string>> Get();
        void Post();
        void Put(int id);
        void Delete(int id);
    }
}

