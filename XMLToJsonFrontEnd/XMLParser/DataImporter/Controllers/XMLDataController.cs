﻿using System;
using System.Xml;
using XMLParserService.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using XMLParserService.Helper;
using XMLDataDomain.Model;
using System.Text.Json;
using static XMLParserService.Helper.XMLHelper;

namespace XMLParser.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class XMLDataController : ControllerBase
    {

        private readonly IXMLParserService _XMLParserService;

        public XMLDataController(IXMLParserService XMLParserService)
        {
            _XMLParserService = XMLParserService;
        }

        [Route("SumValues")]
        public string Sum()
        {
            return "gourav sum valyes";
        }


        [Route("OP")]
        [HttpGet]
        public string TakeData()
        {
            XmlDocument myXml = new XmlDocument();
            myXml.Load(@"..\fakeData.xml");

            List<string> ResultData = new List<string>();

            foreach (var item in SustainParameters())
            {
                ResultData.Add(XMlParserHelper(myXml, item));
            }

            var model2 = new List<XMLModel> {
                new XMLModel {
                    Material = ResultData[0],
                    MfgCo2 = ResultData[1],
                    TransportCo2 = ResultData[2],
                    PaperCo2 = ResultData[3],
                    ConsumablesCo2 = ResultData[4],
                    EolCo2 = ResultData[5],
                    TotalCo2 = ResultData[6],
                    EnergyStar = ResultData[7],
                    EnergyCost = ResultData[8],
                    Currency = ResultData[9]
                },
               };

            var jsons = JsonSerializer.Serialize(model2);
            return jsons.ToString();
        }


        [HttpGet]
        [Route("XMLTOJSON")]
        public string XmlParse()
        {
            XmlDocument myXml = new XmlDocument();
            myXml.Load(@"..\fakeData.xml");

            foreach (var item in XMLHelper.SustainParameters())
            {
                XMlParserHelper(myXml, item);
            }
            return myXml.InnerXml;
        }


        [HttpGet]
        public IEnumerable<IEnumerable<string>> Get()
        {
            return _XMLParserService.Get();
        }

        [HttpPost]
        public void GetData()
        {
            _XMLParserService.Post();
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

    }
}

